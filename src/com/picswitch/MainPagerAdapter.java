package com.picswitch;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class MainPagerAdapter extends FragmentPagerAdapter {

	public MainPagerAdapter(FragmentManager fm) {
		super(fm);
	}

	@Override
	public Fragment getItem(int arg0) {
		switch (arg0){
		case 0:
			return new AdditionalPuzzlesFragment();
		case 1:
			return new MainPageFragment();
		}
		return null;
	}

	@Override
	public int getCount() {
		return 2;
	}

}
