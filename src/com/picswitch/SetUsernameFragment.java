package com.picswitch;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;

public class SetUsernameFragment extends Fragment {
	
	Context mContext;
	EditText et;
	InputMethodManager imm;
	
	public interface SetUsernameFragmentListener{
		public void onSetUsernameBackClicked();
	}
	
	private SetUsernameFragmentListener mSetUsernameListener;

	public SetUsernameFragment() {
	}
	
	public static SetUsernameFragment newInstance(Bundle configOptions){
		SetUsernameFragment usernameFrag = new SetUsernameFragment();
		Bundle args = new Bundle(configOptions);
		usernameFrag.setArguments(args);
		return usernameFrag;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View mView;
		mView = inflater.inflate(R.layout.set_username_fragment, container, false);
		mContext = container.getContext();
		imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
		
		et = (EditText) mView.findViewById(R.id.set_username_field);
				
		ImageView leftArrow = (ImageView) mView.findViewById(R.id.left_arrow_set_username);
		leftArrow.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				imm.hideSoftInputFromWindow(et.getWindowToken(), 0);
				mSetUsernameListener.onSetUsernameBackClicked();
			}
		});
		
		return mView;
	}
	
	@Override
	public void onAttach(Activity activity){
		super.onAttach(activity);
		if(activity instanceof SetUsernameFragmentListener){
			mSetUsernameListener = (SetUsernameFragmentListener) activity;
		}else{
			throw new IllegalArgumentException(
					"Activity must implement SetUsernameFragmentListener");
		}
	}

}
