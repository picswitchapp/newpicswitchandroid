package com.picswitch;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * A simple {@link Fragment} subclass.
 *
 */
public class FriendsFragment extends Fragment {
	
	public interface FriendsFragmentListener{
		public void onFriendsFragmentAddFriendsClicked();
		public void onFriendsFragmentBackClicked();
	}

	private FriendsFragmentListener mFriendsListener;
	
	public FriendsFragment() {
	}
	
	public static FriendsFragment newInstance(Bundle configOptions){
		FriendsFragment friendsFrag = new FriendsFragment();
		Bundle args = new Bundle(configOptions);
		friendsFrag.setArguments(args);
		return friendsFrag;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		View mView;
		mView = inflater.inflate(R.layout.friends_fragment, container, false);
		
		return mView;
	}
	
	@Override
	public void onAttach(Activity activity){
		super.onAttach(activity);
		if(activity instanceof FriendsFragmentListener){
			mFriendsListener = (FriendsFragmentListener) activity;
		}else{
			throw new IllegalArgumentException(
					"Activity must implement FriendsFragmentListener");
		}
	}

}
