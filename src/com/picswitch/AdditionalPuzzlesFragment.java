package com.picswitch;

import java.util.List;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

/**
 * A simple {@link Fragment} subclass.
 *
 */
public class AdditionalPuzzlesFragment extends Fragment {

	private ListView mList;
	private AdditionalPuzzlesAdapter mAdapter;
	private List<ParseObject> mCategories;
	private Context mContext;

	private boolean isQuerying = true;
	
	View mView;

	public AdditionalPuzzlesFragment() {
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		mContext = container.getContext();

		mView = inflater.inflate(R.layout.additional_puzzles_fragment, container, false);
		
		mList = (ListView) mView.findViewById(R.id.additional_puzzles_list_view);
		mList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				
			}
		});

		new AdditionalPuzzlesQuery().execute();
		
		return mView;
	}
	
	@Override
	public void onResume(){
		super.onResume();
		
		List<String> userSolved = ParseUser.getCurrentUser().getList(ParseConstants.KEY_PUZZLES_SOLVED);
		//do work on userSolved to determine which puzzles the user has already solved.  
	}

	public class AdditionalPuzzlesQuery extends AsyncTask<Void,Boolean,Void>{

		@Override
		protected Void doInBackground(Void... params) {
			ParseQuery<ParseObject> mQuery = new ParseQuery<ParseObject>(ParseConstants.CLASS_ADDITIONAL_PUZZLE_CATEGORIES);
			mQuery.findInBackground(new FindCallback<ParseObject>() {
				@Override
				public void done(List<ParseObject> categories, ParseException e) {
					if(e == null){
						mCategories = categories;
						publishProgress(true);
					}else{
						e.printStackTrace();
					}
				}
			});

			return null;
		}

		@Override
		protected void onProgressUpdate(Boolean... params){
			if(params[0]){
				mView.findViewById(R.id.additional_puzzles_loading).setVisibility(View.GONE);
				isQuerying = false;
				System.out.println("inside on progress");
				System.out.println("mCategories.size() = " + mCategories.size());
				mAdapter = new AdditionalPuzzlesAdapter(mContext, mCategories);
				mList.setAdapter(mAdapter);
			}
		}

	}

}
