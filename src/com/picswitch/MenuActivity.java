package com.picswitch;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.picswitch.AddFriendsFragment.AddFriendsFragmentListener;
import com.picswitch.AddFriendsPhoneNumberFragment.AddFriendsPhoneNumberListener;
import com.picswitch.AddFriendsUsernameFragment.AddFriendsUsernameListener;
import com.picswitch.ChangeNameFragment.ChangeNameFragmentListener;
import com.picswitch.ChangePasswordFragment.ChangePasswordFragmentListener;
import com.picswitch.FriendsFragment.FriendsFragmentListener;
import com.picswitch.SetPhoneNumberFragment.SetPhoneNumberFragmentListener;
import com.picswitch.SetUsernameFragment.SetUsernameFragmentListener;
import com.picswitch.SettingsFragment.SettingsFragmentListener;
import com.picswitch.StatisticsFragment.StatisticsFragmentListener;
import com.picswitch.UploadProfilePicFragment.UploadProfilePicFragmentListener;

public class MenuActivity extends FragmentActivity implements AddFriendsFragmentListener,
FriendsFragmentListener, SettingsFragmentListener, StatisticsFragmentListener,
ChangeNameFragmentListener, ChangePasswordFragmentListener, SetUsernameFragmentListener,
UploadProfilePicFragmentListener, SetPhoneNumberFragmentListener, AddFriendsPhoneNumberListener,
AddFriendsUsernameListener{

	private final int fragmentContainer = android.R.id.content;

	private Bundle configOptions;

	private AddFriendsFragment mAddFriendsFragment;
	private FriendsFragment mFriendsFragment;
	private SettingsFragment mSettingsFragment;
	private StatisticsFragment mStatsFragment;
	private UploadProfilePicFragment mUploadPicFragment;

	//variable to keep track of the first page to display,
	//depends on which option the user clicks in menu
	private int firstPage;

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
			Window window = getWindow();
			window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
			window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
			window.setStatusBarColor(Color.rgb(32, 99, 99));
		}

		//get information from intent on which fragment to open first,
		configOptions = getMergedOptions();
		firstPage = getIntent().getExtras().getInt(ParseConstants.KEY_MENU_FIRST_PAGE);

		if(savedInstanceState == null){
			//open the correct first fragment based on where the user clicked
			//also sets up any arguments that need to be passed to each fragment

			switch(firstPage){
			case ParseConstants.PAGE_ADD_FRIENDS:{
				mAddFriendsFragment = AddFriendsFragment.newInstance(configOptions);
				getSupportFragmentManager().beginTransaction().add(fragmentContainer,
						mAddFriendsFragment, "addFriendsFrag").commit();
				break;
			}
			case ParseConstants.PAGE_FRIENDS:{
				mFriendsFragment = FriendsFragment.newInstance(configOptions);
				getSupportFragmentManager().beginTransaction().add(fragmentContainer,
						mFriendsFragment, "friendsFrag").commit();
				break;
			}
			case ParseConstants.PAGE_SETTINGS:{
				mSettingsFragment = SettingsFragment.newInstance(configOptions);
				getSupportFragmentManager().beginTransaction().add(fragmentContainer,
						mSettingsFragment, "settingsFrag").commit();
				break;
			}
			case ParseConstants.PAGE_STATS:{
				mStatsFragment = StatisticsFragment.newInstance(configOptions);
				getSupportFragmentManager().beginTransaction().add(fragmentContainer,
						mStatsFragment, "statsFrag").commit();
				break;
			}
			default:{
				//toast -> error on opening the intent;
				break;
			}
			}
		}
	}

	private Bundle getMergedOptions() {
		// Read activity metadata from AndroidManifest.xml
		ActivityInfo activityInfo = null;
		try {
			activityInfo = getPackageManager().getActivityInfo(
					this.getComponentName(), PackageManager.GET_META_DATA);
		} catch (NameNotFoundException e) {

		}

		// The options specified in the Intent (from ParseLoginBuilder) will
		// override any duplicate options specified in the activity metadata
		Bundle mergedOptions = new Bundle();
		if (activityInfo != null && activityInfo.metaData != null) {
			mergedOptions.putAll(activityInfo.metaData);
		}
		if (getIntent().getExtras() != null) {
			mergedOptions.putAll(getIntent().getExtras());
		}

		return mergedOptions;
	}

	public void onBackPressed(){
		//have to hande all the types of fragments here
		Fragment currentFragment = getSupportFragmentManager().findFragmentById(fragmentContainer);
		if(currentFragment instanceof SettingsFragment ||
				currentFragment instanceof AddFriendsFragment ||
				currentFragment instanceof FriendsFragment ||
				currentFragment instanceof StatisticsFragment){

			//recycle the cached fragments -> will need more research to find good solution, avoiding potential out of memory errors

			MenuActivity.this.finish();
		}else if(currentFragment instanceof ChangeNameFragment){
			onChangeNameFragmentBackClicked();
		}else if(currentFragment instanceof ChangePasswordFragment){
			onChangePasswordBackClicked();
		}else if(currentFragment instanceof SetUsernameFragment){
			onSetUsernameBackClicked();
		}else if(currentFragment instanceof UploadProfilePicFragment){
			onUploadProfilePicBackClicked();
		}else if(currentFragment instanceof SetPhoneNumberFragment){
			onSetPhoneNumberBackClicked();
		}else if(currentFragment instanceof AddFriendsPhoneNumberFragment){
			onAddFriendsPhoneNumberBackClicked();
		}else if(currentFragment instanceof AddFriendsUsernameFragment){
			onAddFriendsUsernameBackClicked();
		}
	}

	//add friends methods
	@Override
	public void onAddFriendsBackClicked() {
		if(firstPage == ParseConstants.PAGE_ADD_FRIENDS){
			//first page was add friends, so finish the activity
			MenuActivity.this.finish();
		}else{
			//first page was friends fragment, so return to that
			if(mFriendsFragment == null){
				mFriendsFragment = FriendsFragment.newInstance(configOptions);
			}

			getSupportFragmentManager().beginTransaction().replace(fragmentContainer,
					mFriendsFragment).commit();
		}
	}

	@Override
	public void onAddFriendsPhoneNumberClicked() {
		// go to phone number add friends fragment
		
		//first check if they have a phone number
		getSupportFragmentManager().beginTransaction().add(fragmentContainer,
				AddFriendsPhoneNumberFragment.newInstance(configOptions)).commit();
	}

	@Override
	public void onAddFriendsUsernameClicked() {
		// go to username search fragment
		getSupportFragmentManager().beginTransaction().add(fragmentContainer,
				AddFriendsUsernameFragment.newInstance(configOptions)).commit();
	}
	
	//add friends phone number fragment methods
	@Override
	public void onAddFriendsPhoneNumberBackClicked() {
		if(mAddFriendsFragment == null){
			mAddFriendsFragment = AddFriendsFragment.newInstance(configOptions);
		}
		getSupportFragmentManager().beginTransaction().replace(fragmentContainer,
				mAddFriendsFragment).commit();		
	}
	
	//add friends username fragment methods
	@Override
	public void onAddFriendsUsernameBackClicked() {
		//go back to addFriendsFragment
		if(mAddFriendsFragment == null){
			mAddFriendsFragment = AddFriendsFragment.newInstance(configOptions);
		}
		getSupportFragmentManager().beginTransaction().replace(fragmentContainer,
				mAddFriendsFragment).commit();	
	}
	
	@Override
	public void onSearchUsername(String toSearch){
		//query for the entered search term.
	}
	
	//friends / requests methods
	@Override
	public void onFriendsFragmentAddFriendsClicked() {
		//either reload the previously used addfriends fragment or create a new one
		if(mAddFriendsFragment == null){
			mAddFriendsFragment = AddFriendsFragment.newInstance(configOptions);
		}
		getSupportFragmentManager().beginTransaction().add(fragmentContainer,
				mAddFriendsFragment).commit();
	}

	@Override
	public void onFriendsFragmentBackClicked() {
		MenuActivity.this.finish();
	}

	//settings methods
	//handle navigation, correct flow for each click
	@Override
	public void onSettingsBackClicked() {
		// TODO Auto-generated method stub
		MenuActivity.this.finish();
	}

	@Override
	public void onChangeNameClicked() {
		getSupportFragmentManager().beginTransaction().add(fragmentContainer,
				ChangeNameFragment.newInstance(configOptions)).commit();
	}

	@Override
	public void onChangePasswordClicked() {
		getSupportFragmentManager().beginTransaction().add(fragmentContainer,
				ChangePasswordFragment.newInstance(configOptions)).commit();
	}

	@Override
	public void onSetPhoneNumberClicked() {
		getSupportFragmentManager().beginTransaction().add(fragmentContainer,
				SetPhoneNumberFragment.newInstance(configOptions)).commit();		
	}

	@Override
	public void onSetUsernameClicked() {
		getSupportFragmentManager().beginTransaction().add(fragmentContainer,
				SetUsernameFragment.newInstance(configOptions)).commit();
	}

	@Override
	public void onUploadProfileImageClicked() {
		if(mUploadPicFragment == null){
			mUploadPicFragment = UploadProfilePicFragment.newInstance(configOptions);
		}
		getSupportFragmentManager().beginTransaction().add(fragmentContainer,
				mUploadPicFragment).commit();
	}

	@Override
	public void onFaqsClicked(){
		Intent intent = new Intent(Intent.ACTION_VIEW);
		intent.setData(Uri.parse("http://www.picswitchapp.com/faqs/"));
		startActivity(intent);
	}

	@Override
	public void onTermsClicked(){
		Intent intent = new Intent(Intent.ACTION_VIEW);
		intent.setData(Uri.parse("http://www.picswitchapp.com/terms/"));
		startActivity(intent);
	}

	@Override
	public void onPrivacyPolicyClicked(){
		Intent intent = new Intent(Intent.ACTION_VIEW);
		intent.setData(Uri.parse("http://www.picswitchapp.com/privacy/"));
		startActivity(intent);
	}

	@Override
	public void onContestRulesClicked(){
		Intent intent = new Intent(Intent.ACTION_VIEW);
		intent.setData(Uri.parse("http://www.picswitchapp.com/contest-rules/"));
		startActivity(intent);
	}

	@Override
	public void onLogOutClicked() {
		AlertDialog.Builder mBuilder = new AlertDialog.Builder(MenuActivity.this);
		mBuilder.setTitle("Log out");
		mBuilder.setMessage("Are you sure you want to log out?");
		mBuilder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				ParseUser.logOut();
				Intent intent = new Intent(MenuActivity.this, MainActivity.class);
				startActivity(intent);
				dialog.cancel();
				MenuActivity.this.finish();
			}
		});
		mBuilder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();
			}
		});
		AlertDialog mDialog = mBuilder.create();
		mDialog.show();
	}

	//statistics methods
	@Override
	public void onStatisticsBackClicked() {

	}

	//change name fragment methods
	@Override
	public void onChangeNameFragmentBackClicked() {
		if(mSettingsFragment == null){
			mSettingsFragment = SettingsFragment.newInstance(configOptions);
		}
		getSupportFragmentManager().beginTransaction().replace(fragmentContainer,
				mSettingsFragment).commit();
	}

	@Override
	public void onConfirmNameChangeClicked(final String newFirstName, final String newLastName) {
		//handle the user actually clicking to confirm a name change	

		if(ParseUser.getCurrentUser().getInt("nameChangeCount") != 1){

			if((newFirstName.length() > 0) && (newLastName.length() > 0)){
				//names are enterred, continue with dialogs
				AlertDialog.Builder mBuilder = new AlertDialog.Builder(MenuActivity.this);
				mBuilder.setTitle("Warning");
				mBuilder.setMessage("You are only allowed to change your name once on picswitch."
						+ "  Are you sure you want to make this change?");
				mBuilder.setPositiveButton("Yes, change my name", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						//second confirm dialog					
						dialog.cancel();

						AlertDialog.Builder mmBuilder = new AlertDialog.Builder(MenuActivity.this);
						mmBuilder.setTitle("Confirm name change");
						mmBuilder.setMessage("Your name will be chaged to " + newFirstName + " " + newLastName + "." + 
								"  You will not be able to change your name again.  Continue?");
						mmBuilder.setPositiveButton("Yes, change my name", new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								//actually change their name
								ParseUser mUser = ParseUser.getCurrentUser();
								mUser.put(ParseConstants.KEY_FIRST_NAME, newFirstName);
								mUser.put(ParseConstants.KEY_LAST_NAME, newLastName);
								mUser.put("nameChangeCount", 1);
								mUser.saveInBackground(new SaveCallback() {
									@Override
									public void done(ParseException e) {
										if(e == null){
											//saved updated successfully
											Toast.makeText(MenuActivity.this, "Name updated successfully", Toast.LENGTH_SHORT).show();
										}else{
											//saving failed
											Toast.makeText(MenuActivity.this, "Updating name failed", Toast.LENGTH_SHORT).show();
										}
										//return to settings page
										onChangeNameFragmentBackClicked();
									}
								});
								dialog.cancel();
							}
						});
						mmBuilder.setNegativeButton("No, don't change my name", new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								dialog.cancel();
								onChangeNameFragmentBackClicked();
							}
						});
						AlertDialog mmDialog = mmBuilder.create();
						mmDialog.show();
					}
				});
				mBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						onChangeNameFragmentBackClicked();
						dialog.cancel();
					}
				});
				AlertDialog mDialog = mBuilder.create();
				mDialog.show();
			}else{
				//entered names are not valid
				AlertDialog.Builder noNamesBuilder = new AlertDialog.Builder(MenuActivity.this);
				noNamesBuilder.setTitle("Please enter both a first and last name");
				noNamesBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
					}
				});
				noNamesBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
						onChangeNameFragmentBackClicked();
					}
				});
				AlertDialog noNamesDialog = noNamesBuilder.create();
				noNamesDialog.show();
			}
		}else{
			//user has already updated name
			AlertDialog.Builder alreadyChanged = new AlertDialog.Builder(MenuActivity.this);
			alreadyChanged.setTitle("You have already changed your name.");
			alreadyChanged.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					onChangeNameFragmentBackClicked();
					dialog.cancel();
				}
			});
			AlertDialog alreadyDialog = alreadyChanged.create();
			alreadyDialog.show();
		}

	}

	//change password fragment
	@Override
	public void onChangePasswordBackClicked() {
		//return to main settings fragment	
		if(mSettingsFragment == null){
			mSettingsFragment = SettingsFragment.newInstance(configOptions);
		}
		getSupportFragmentManager().beginTransaction().replace(fragmentContainer,
				mSettingsFragment).commit();
	}

	@Override
	public void onChangePasswordConfirmClicked(String currentPass, final String newPass, final String confNewPass) {
		//handle the user trying to change passwords

		if((currentPass.length() > 0) && (newPass.length() > 0) && (confNewPass.length() > 0)){

			ParseUser.logInInBackground(ParseUser.getCurrentUser().getUsername(), currentPass, new LogInCallback() {
				@Override
				public void done(ParseUser user, ParseException e) {
					if(e ==null){
						if(user != null){								
							//password is correct
							if(newPass.equals(confNewPass)){
								//passwords match
								if(newPass.length() > 5){
									//new password is valid
									ParseUser mUser = ParseUser.getCurrentUser();
									mUser.setPassword(newPass);
									mUser.saveInBackground(new SaveCallback() {
										@Override
										public void done(ParseException e) {
											if(e == null){
												Toast.makeText(MenuActivity.this, "Successfully changed password", Toast.LENGTH_SHORT).show();
											}else{
												e.printStackTrace();
												Toast.makeText(MenuActivity.this, "Changing password failed", Toast.LENGTH_SHORT).show();
											}
											onChangePasswordBackClicked();
										}
									});
								}else{
									//new passwords are not valid
									AlertDialog.Builder mBuilder = new AlertDialog.Builder(MenuActivity.this);
									mBuilder.setTitle("New password is not valid");
									mBuilder.setMessage("Passwords must be at least 6 characters in length.");
									mBuilder.setPositiveButton("Try again", new DialogInterface.OnClickListener() {
										@Override
										public void onClick(DialogInterface dialog, int which) {
											dialog.cancel();
										}
									});
									mBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
										@Override
										public void onClick(DialogInterface dialog, int which) {
											dialog.cancel();
											onChangePasswordBackClicked();
										}
									});
									AlertDialog mDialog = mBuilder.create();
									mDialog.show();
								}

							}else{
								//new passwords dont match
								AlertDialog.Builder mBuilder = new AlertDialog.Builder(MenuActivity.this);
								mBuilder.setTitle("New passwords don't match");
								mBuilder.setPositiveButton("Try again", new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog, int which) {
										dialog.cancel();
									}
								});
								mBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog, int which) {
										dialog.cancel();
										onChangePasswordBackClicked();
									}
								});
								AlertDialog mDialog = mBuilder.create();
								mDialog.show();
							}
						}else{
							//password is incorrect
							AlertDialog.Builder mBuilder = new AlertDialog.Builder(MenuActivity.this);
							mBuilder.setTitle("Current password is incorrect");
							mBuilder.setPositiveButton("Try again", new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog, int which) {
									dialog.cancel();
								}
							});
							mBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog, int which) {
									onChangePasswordBackClicked();
									dialog.cancel();
								}
							});
							AlertDialog mDialog = mBuilder.create();
							mDialog.show();
						}
					}else{
						e.printStackTrace();

						//password is incorrect
						AlertDialog.Builder mBuilder = new AlertDialog.Builder(MenuActivity.this);
						mBuilder.setTitle("Current password is incorrect");
						mBuilder.setPositiveButton("Try again", new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								dialog.cancel();
							}
						});
						mBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								dialog.cancel();
								onChangePasswordBackClicked();
							}
						});
						AlertDialog mDialog = mBuilder.create();
						mDialog.show();
					}
				}
			});
		}else{
			//handle case of empty entries here
			AlertDialog.Builder mBuilder = new AlertDialog.Builder(MenuActivity.this);
			mBuilder.setTitle("Invalid Passwords");
			mBuilder.setMessage("Enter your current and desired passwords");
			mBuilder.setPositiveButton("Try again", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.cancel();
					onChangePasswordBackClicked();
				}
			});
			mBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.cancel();
					onChangePasswordBackClicked();
				}
			});
			AlertDialog mDialog = mBuilder.create();
			mDialog.show();
		}		
	}

	@Override
	public void onSetPhoneNumberBackClicked() {
		//return to main settings fragment
		if(mSettingsFragment == null){
			mSettingsFragment = SettingsFragment.newInstance(configOptions);
		}
		getSupportFragmentManager().beginTransaction().replace(fragmentContainer,
				mSettingsFragment).commit();
	}

	//set username fragment
	@Override
	public void onSetUsernameBackClicked() {
		//return to main settings fragment
		if(mSettingsFragment == null){
			mSettingsFragment = SettingsFragment.newInstance(configOptions);
		}
		getSupportFragmentManager().beginTransaction().replace(fragmentContainer,
				mSettingsFragment).commit();
	}

	//upload profile pic fragment methods
	@Override
	public void onUploadProfilePicBackClicked() {
		//return to main settings fragment
		if(mSettingsFragment == null){
			mSettingsFragment = SettingsFragment.newInstance(configOptions);
		}
		getSupportFragmentManager().beginTransaction().replace(fragmentContainer,
				mSettingsFragment).commit();
	}

}
