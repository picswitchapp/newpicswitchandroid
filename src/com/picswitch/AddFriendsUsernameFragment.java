package com.picswitch;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class AddFriendsUsernameFragment extends Fragment {
	
	public interface AddFriendsUsernameListener{
		public void onAddFriendsUsernameBackClicked();
		public void onSearchUsername(String toSearch);
	}
	
	private AddFriendsUsernameListener mAddFriendsUsernameListener;

	public AddFriendsUsernameFragment() {
	}
	
	public static AddFriendsUsernameFragment newInstance(Bundle configOptions){
		AddFriendsUsernameFragment usernameFrag = new AddFriendsUsernameFragment();
		Bundle args = new Bundle(configOptions);
		usernameFrag.setArguments(args);
		return usernameFrag;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View mView;
		mView = inflater.inflate(R.layout.add_friends_username_fragment, container, false);
		
		return mView;
	}
	
	@Override
	public void onAttach(Activity activity){
		super.onAttach(activity);
		if(activity instanceof AddFriendsUsernameListener){
			mAddFriendsUsernameListener = (AddFriendsUsernameListener) activity;
		}else{
			throw new IllegalArgumentException(
					"Activity must implement AddFriendsUsernameListener");
		}
	}

}
