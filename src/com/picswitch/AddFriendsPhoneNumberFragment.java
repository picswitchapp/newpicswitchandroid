package com.picswitch;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * A simple {@link Fragment} subclass.
 *
 */
public class AddFriendsPhoneNumberFragment extends Fragment {
	
	public interface AddFriendsPhoneNumberListener{
		public void onAddFriendsPhoneNumberBackClicked();
		//method for passing numbers to activity for sorting with cloud function
		//method for actually adding friends, passing object id
		//method for opening intent to send text to someone, just pass the phone number
	}
	
	private AddFriendsPhoneNumberListener mAddFriendsPhoneListener;

	public AddFriendsPhoneNumberFragment() {
	}
	
	public static AddFriendsPhoneNumberFragment newInstance(Bundle configOptions){
		AddFriendsPhoneNumberFragment phoneNumberFragment = new AddFriendsPhoneNumberFragment();
		Bundle args = new Bundle(configOptions);
		phoneNumberFragment.setArguments(args);
		return phoneNumberFragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View mView;
		mView = inflater.inflate(R.layout.add_friends_phone_number_fragment, container, false);
		
		//get phone numbers from contacts
		
		//call method to sort the phone numbers
		
		//set adapters to display the names and numbers in listview
		
		//handle child clicks, keep track of which name a numbers have been selected
		
		//on bottom button click, call methods to add friends or send invite texts
		
		return mView;
	}
	
	@Override
	public void onAttach(Activity activity){
		super.onAttach(activity);
		if(activity instanceof AddFriendsPhoneNumberListener){
			mAddFriendsPhoneListener = (AddFriendsPhoneNumberListener) activity;
		}else{
			throw new IllegalArgumentException(
					"Activity must implement AddFriendsPhoneNumberListener");
		}
	}

}
