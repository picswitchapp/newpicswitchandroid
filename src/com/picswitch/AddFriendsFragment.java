package com.picswitch;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

/**
 * A simple {@link Fragment} subclass.
 *
 */
public class AddFriendsFragment extends Fragment {
	
	public interface AddFriendsFragmentListener{
		public void onAddFriendsBackClicked();
		public void onAddFriendsPhoneNumberClicked();
		public void onAddFriendsUsernameClicked();
	}
	
	private AddFriendsFragmentListener addFriendsListener;

	public AddFriendsFragment() {
	}
	
	public static AddFriendsFragment newInstance(Bundle configOptions){
		AddFriendsFragment addFrag = new AddFriendsFragment();
		Bundle args = new Bundle(configOptions);
		addFrag.setArguments(args);
		return addFrag;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		View mView;
		mView = inflater.inflate(R.layout.add_friends_fragment, container, false);
		
		ImageView leftArrow = (ImageView) mView.findViewById(R.id.left_arrow_add_friends_selector);
		leftArrow.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				addFriendsListener.onAddFriendsBackClicked();
			}
		});
		
		RelativeLayout byPhone = (RelativeLayout) mView.findViewById(R.id.by_phone_layout);
		byPhone.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				addFriendsListener.onAddFriendsPhoneNumberClicked();
			}
		});
		
		RelativeLayout byUsername = (RelativeLayout) mView.findViewById(R.id.add_username_layout);
		byUsername.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				addFriendsListener.onAddFriendsUsernameClicked();
			}
		});
		
		return mView;
	}
	
	@Override
	public void onAttach(Activity activity){
		super.onAttach(activity);
		
		if(activity instanceof AddFriendsFragmentListener){
			addFriendsListener = (AddFriendsFragmentListener) activity;
		}else{
			throw new IllegalArgumentException(
					"Activity must implement SolveFragmentListener");
		}
	}

}
