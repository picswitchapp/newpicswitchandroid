package com.picswitch;

public class ParseConstants {

	//Class Name
	public static final String CLASS_MESSAGES = "Messages";
	public static final String CLASS_FEATURED_PUZZLES = "FeaturedPuzzles";
	public static final String CLASS_ADVENTURE_AND_SPORTS = "AdventureAndSports";
	public static final String CLASS_ANIMALS = "Animals";
	public static final String CLASS_ARCHITECTURE = "Architecture";
	public static final String CLASS_CITIES = "Cities";
	public static final String CLASS_JOKES_AND_PUNS = "JokesAndPuns";
	public static final String CLASS_LANDSCAPES = "LandScapes";
	public static final String CLASS_MODERN_CULTURE = "ModernCulture";
	public static final String CLASS_NATURE = "Nature";
	public static final String CLASS_ROMANTIC = "Romantic";
	public static final String CLASS_TECHNOLOGY_AND_TRANSPORTATION = "TechnologyAndTransportation";
	public static final String CLASS_VINTAGE = "Vintage";
	public static final String CLASS_ADDITIONAL_PUZZLE_CATEGORIES = "AdditionalPuzzleCategories";
	public static final String CLASS_PUZZLES_SOLVED = "puzzlesSolved";
	public static final String CLASS_USER = "_User";

	//Field Names
	public static final String KEY_USERNAME = "username";
	public static final String KEY_FRIENDS_RELATION = "friendsRelation";
	public static final String KEY_RECIPIENTS_IDS = "recipientIds";
	public static final String KEY_SENDER_ID = "senderId";
	public static final String KEY_SENDER_NAME = "senderName";
	public static final String KEY_SENDER_FIRST_NAME = "firstName";
	public static final String KEY_LAST_NAME = "lastName";
	public static final String KEY_FILE = "file";
	public static final String KEY_PHOTO = "photo";
	public static final String KEY_FILE_TYPE = "fileType";
	public static final String KEY_CREATED_AT = "createdAt";
	public static final String KEY_MESSAGE_ID = "objectId";
	public static final String KEY_RECIPIENTS = "recipients";
	public static final String KEY_SENDER = "sender";
	public static final String KEY_FIRST_NAME = "firstName";
	public static final String KEY_PHONE_NUMBER = "phoneNumber";
	public static final String KEY_PREVIEW_INDEX = "previewIndex";
	
	public static final String KEY_TIME_LIMIT = "timeLimit";
	public static final String KEY_CURRENT_NEW_INDEX = "currentNew";
	
	//addtionalPuzzles
	public static final String KEY_PUZZLE_CLASS = "puzzleClass";
	public static final String KEY_COMPLETED_USERS = "completedUsers";
	public static final String KEY_PUZZLE_COUNTER = "puzzleCounter";
	public static final String KEY_TIMER = "puzzleCounter";
	public static final String KEY_SWITCHES = "puzzleCounter";
	public static final String KEY_ADDITIONAL_PUZZLE_TEMP = "additionalPuzzleTemp";
	public static final String KEY_ADDITIONAL_DATA = "additionalData";
	public static final String KEY_OBJECT_ID = "objectId";
	public static final String KEY_PUZZLES_SOLVED = "puzzlesSolved";
	
	public static final String KEY_CATEGORY_NAME = "categoryName";
	public static final String KEY_CATEGORY_DESCRIPTION = "categoryDescription";
	public static final String KEY_USERS = "users";
	public static final String KEY_TEMP_SOLVED = "tempSolved";
	
	public static final String KEY_TEMP_SKIPPED = "tempSkipped";
	public static final String KEY_CATEGORY_TITLE = "categoryTitle";
	public static final String KEY_CATEGORY_TITLES = "categoryTitles";
	public static final String KEY_CLASS_NAMES = "classNames";
	public static final String KEY_CURRENT_INT = "currentInt";
	public static final String KEY_TEMP_DATA = "tempData";
	
	//new challenges
	public static final String KEY_NEW_CHALLENGES_TEMP = "additionalPuzzleTemp"; //same as additional to override previous images and save on space


	//featured challenges
	public static final String KEY_COMPANY_NAME = "companyName";
	public static final String KEY_FEATURED_SOLVED = "featuredSolved";


	//file types
	public static final String TYPE_IMAGE = "image";



	//Captions
	public static final String CAPTION_FIRST = "firstCaption";
	public static final String CAPTION_SECOND = "secondCaption";
	public static final String KEY_SOLVE_STATUS = "solveStatus";
	public static final String SOLVED = "solved";
	public static final String NOT_SOLVED = "not solved";



	//CloudFunctions
	public static final String CLOUD_STATISTICS = "statistics";
	public static final String ADDITIONAL_STATISTICS = "additional";

	//statistics
	public static final String KEY_STATS_USERNUMBERS = "userNumbers";
	public static final String CLASS_STATISTICS = "Statistics";
	
	public static final String KEY_STATS_NOLIMIT = "noLimit";
	public static final String KEY_STATS_VERYHARD = "veryHard";
	public static final String KEY_STATS_HARD = "hard";
	public static final String KEY_STATS_NORMAL = "normal";
	public static final String KEY_STATS_EASY = "easy";
	
	public static final String KEY_STATS_USERNAME = "userName";
	public static final String KEY_STATS_PUZZLEDIFFICULTY = "puzzleDifficulty";
	public static final String KEY_STATS_SWITCHCOUNTER = "switches";
	public static final String KEY_STATS_TIME = "time";
	public static final String KEY_STATS_CATEGORY = "category";
	public static final String KEY_STATS_NEW = "newPuzzles";
	public static final String KEY_STATS_ADDITIONAL = "additional";
	
	
	//Profile Keys
	public static final String KEY_PICSWITCH_USERNAME = "picswitchUsername";
	public static final String KEY_PROFILE_PICTURE = "profilePicture";
	
	
	public static final String ACCEPT = "accept";
	public static final String DENY = "deny";
	public static final int NOTIFICATION_TAB = 1;
	public static final int NOTIFICATION_FRIEND = 2;
	
	//constants for type of message in ImageActivity
	public static final String KEY_MESSAGE_TYPE = "messageType";
	public static final String NEW_MESSAGE = "newMessage";
	public static final String FEATURED_MESSAGE = "featuredMessage";
	public static final String ADDITIONAL_MESSAGE = "additionalMessage";
	
	//menu activity constant strings
	public static final String KEY_MENU_FIRST_PAGE = "menuFirstPage";
	public static final int PAGE_ADD_FRIENDS = 0;
	public static final int PAGE_FRIENDS = 1;
	public static final int PAGE_SETTINGS = 2;
	public static final int PAGE_STATS = 3;
	
}