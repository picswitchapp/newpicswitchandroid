package com.picswitch;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

public class SetPhoneNumberFragment extends Fragment {
	
	public interface SetPhoneNumberFragmentListener{
		public void onSetPhoneNumberBackClicked();
	}
	
	private SetPhoneNumberFragmentListener mPhoneNumberListener;

	public SetPhoneNumberFragment() {
	}
	
	public static SetPhoneNumberFragment newInstance(Bundle configOptions){
		SetPhoneNumberFragment phoneFrag = new SetPhoneNumberFragment();
		Bundle args = new Bundle(configOptions);
		phoneFrag.setArguments(args);
		return phoneFrag;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View mView;
		mView = inflater.inflate(R.layout.set_phone_number_fragment, container, false);
		
		ImageView leftArrow = (ImageView) mView.findViewById(R.id.left_arrow_pn);
		leftArrow.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				mPhoneNumberListener.onSetPhoneNumberBackClicked();
			}
		});
		
		return mView;
	}
	
	@Override
	public void onAttach(Activity activity){
		super.onAttach(activity);
		if(activity instanceof SetPhoneNumberFragmentListener){
			mPhoneNumberListener = (SetPhoneNumberFragmentListener) activity;
		}else{
			throw new IllegalArgumentException(
					"Activity must implement SetPhoneNumberFragmentListener");
		}
	}

}
