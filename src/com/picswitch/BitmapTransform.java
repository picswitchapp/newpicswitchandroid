package com.picswitch;

import android.graphics.Bitmap;

import com.squareup.picasso.Transformation;

public class BitmapTransform implements Transformation{
	
	private int previewIndex;
	
	public BitmapTransform(int previewIndex){
		this.previewIndex = previewIndex;
	}

	@Override
	public String key() {
		return "MessageThumbnails";
	}

	@Override
	public Bitmap transform(Bitmap source) {
		
		int x;
		int y;
		
		int width = source.getWidth(); //original bitmap dimensions
		int height = source.getHeight();
		
		switch(previewIndex){
		case 0:{
			x = 0;
			y = 0;
			break;
		}
		case 1:{
			x = width / 3;
			y = 0;
			break;
		}
		case 2:{
			x = (2 * width) / 3;
			y = 0;
			break;
		}
		case 3:{
			x = 0;
			y = height / 3;
			break;
		}
		case 4:{
			x = width / 3;
			y = height / 3;
			break;
		}
		case 5:{
			x = (2 * width) / 3;
			y = height / 3;
			break;
		}
		case 6:{
			x = 0;
			y = (2 * height) / 3;
			break;
		}
		case 7:{
			x = width / 3;
			y = (2 * height) / 3;
			break;
		}
		case 8:{
			x = (2 * width) / 3;	
			y = (2 * height) / 3;
			break;
		}
		default:{ //use middle tile
			x = width / 3;
			y = height / 3;
			break;
		}
		}
		
		int outWidth = (int) ((source.getWidth()) / 3); //desired width of the thumbnail
	    int outHeight = (int) ((source.getHeight()) / 3); //desired height of the thumbnail
		
	    Bitmap result = Bitmap.createBitmap(source, x, y, outWidth, outHeight);
	    if (result != source) {
	      source.recycle();
	    }
	    return result;
	  }
}
