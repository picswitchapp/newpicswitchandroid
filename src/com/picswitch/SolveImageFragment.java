package com.picswitch;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.Display;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * A simple {@link Fragment} subclass.
 *
 */
public class SolveImageFragment extends Fragment {

	private ImageView image1, image2, image3,image4,image5,image6, image7,image8,image9;
	private GestureDetector gestureDetector;
	int animationToRun = R.anim.fadeout; //animation to be used when user touches PicSwitch
	public int count=0, switchCounter = 0;
	//images of the PicSwitch
	Bitmap img, img0, img1, img2,img3,img4,img5,img6, img7, img8; 
	ArrayList<Integer> images = new ArrayList<Integer>(Arrays.asList(0,1,2,3,4,5,6,7,8));
	ArrayList<Integer> eightStepTmp = new ArrayList<Integer>(Arrays.asList(-1,-1,-1,-1,-1,-1,-1,-1,-1));
	View.OnTouchListener onTouchListener;
	SurfaceHolder s;
	int chunkNumbers =9;
	ImageView placeholder;
	//private String firstCaption, secondCaption, messageID;

	private Context mContext;
	private View mView;

	//temp
	int timeLimit = 45;
	
	//things passed into frag
	String titleName;
	String firstCaption;

	Uri imageUri;
	Bitmap imageBitmap;
	ProgressDialog mDialog;

	//timer vars
	long preciseStartTime;
	long preciseEndTime;
	double timeToDisplay;

	public interface SolveFragmentListener {
		public void onImageSolved();
		public void onSolveNextClicked();
		public void onSolveBackClicked();
	}
	
	public static final String IMAGE_URI = "com.picswitch.SolveImageFragment.IMAGE_URI";
	public static final String TITLE_NAME = "com.picswitch.SolveImageFragment.TITLE_NAME";
	public static final String FIRST_CAPTION = "com.picswitch.SolveImageFragment.FIRST_CAPTION";

	private SolveFragmentListener solveFragListener;

	public SolveImageFragment() {
		// Required empty public constructor
	}

	public static SolveImageFragment newInstance(Bundle configOptions, String mUri, String titleName, String firstCaption){
		SolveImageFragment solveFrag = new SolveImageFragment();
		Bundle args = new Bundle(configOptions);
		args.putString(IMAGE_URI, mUri);
		args.putString(TITLE_NAME, titleName);
		args.putString(FIRST_CAPTION, firstCaption);
		solveFrag.setArguments(args);
		return solveFrag;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		
		Bundle args = getArguments();
		
		imageUri = Uri.parse(args.getString(IMAGE_URI));
		titleName = args.getString(TITLE_NAME);
		firstCaption = args.getString(FIRST_CAPTION);
		
		// Inflate the layout for this fragment
		mView = inflater.inflate(R.layout.solve_image_fragment, container, false);
		mContext = container.getContext();
		//create new eight step thing
		//first step, shuffle the stock array
		Collections.shuffle(images);

		for(int i = 0; i < 9; i++){
			eightStepTmp.set(i, (images.get((images.indexOf(i) + 1) % 9)));
		}

		images = eightStepTmp;
		
		timerTextView = (TextView) mView.findViewById(R.id.open_image_timer);
		
		TextView titleText = (TextView) mView.findViewById(R.id.open_image_title_text);
		titleText.setText(titleName);
		TextView firstCaptionView = (TextView) mView.findViewById(R.id.firstCaption);
		firstCaptionView.setText(firstCaption);
		
		((ImageView) mView.findViewById(R.id.open_image_back_button)).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				solveFragListener.onSolveBackClicked();
			}
		});

		imageViewsHelper();
		imageViewsDimensions();

		makeProgressDialog();
		Picasso.with(mContext).load(imageUri.toString()).into(target);

		return mView;
	}

	@Override
	public void onAttach(Activity activity){
		super.onAttach(activity);

		if(activity instanceof SolveFragmentListener){
			solveFragListener = (SolveFragmentListener) activity;
		}else{
			throw new IllegalArgumentException(
					"Activity must implement SolveFragmentListener");
		}
	}
	
	public void onBackPressed(){
		solveFragListener.onSolveBackClicked();
	}

	/**
	 * Helper function called when PicSwitch has been solved correctly
	 */
	public void done(){
		timerHandler.removeCallbacks(timerRunnable);
		solveFragListener.onImageSolved();
	}

	public void makeProgressDialog(){
		mDialog = new ProgressDialog(mContext);
		mDialog.setMessage("Loading image...");
		mDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		mDialog.setIndeterminate(true);
		mDialog.show();
	}

	Target target = new Target() {

		int timerCount = 0;

		@Override
		public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
			if(mDialog.isShowing()){
				mDialog.cancel();
			}
			imageBitmap = bitmap;
			splitImage(bitmap, chunkNumbers);
			createImageFromBitmap(bitmap, ParseConstants.KEY_NEW_CHALLENGES_TEMP); //save image to disk for viewing in next intent
			imageViewsHelper(); //initializes image views
			imageViewsDimensions(); //sets up the dimensions of the imageviews
			assignImageViewPics(); //add images to imageViews
			setImageViewTouch();

			//preciseStartTime = System.nanoTime();

			//start timer;

			if(timerCount==0){
				startTime = System.currentTimeMillis();
				timerHandler.postDelayed(timerRunnable, 0);
				//TextView timer = (TextView) findViewById(R.id.open_image_timer);
				timerTextView.setBackground(getResources().getDrawable(R.drawable.timerblank));
				timerCount++;
			}

		}

		@Override
		public void onBitmapFailed(Drawable arg0) {

		}

		@Override
		public void onPrepareLoad(Drawable arg0) {


		}
	};

	/**Set up for the timer*/
	TextView timerTextView;
	long startTime = 0;
	int seconds;
	int minutes;
	//runs without a timer by reposting this handler at the end of the runnable
	Handler timerHandler = new Handler();
	Runnable timerRunnable = new Runnable() {
		@SuppressLint("NewApi")
		@Override
		public void run() {
			long millis = System.currentTimeMillis() - startTime;
			seconds = (int) (millis / 1000);
			minutes = seconds / 60;
			seconds = seconds % 60;
			if(timeLimit > 0){
				//defined timelimit, so count down
				if((timeLimit - seconds - 1) >= 0){
					timerTextView.setText(String.format("%02d", timeLimit - seconds));
					timerTextView.setTextAlignment(6);;
					timerHandler.postDelayed(this, 500);
				}else{
					//handle case where not solved within timelimit using dialogbox
					//createFailDialogBox();
					timerHandler.removeCallbacks(timerRunnable);
				}
			}else{
				timerTextView.setText(String.format("%02d", seconds));
				timerTextView.setTextAlignment(6);;
				timerHandler.postDelayed(this, 500);
			}
		}
	};

	public void createFailDialogBox(){

		timerHandler.removeCallbacks(timerRunnable);

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);
		alertDialogBuilder.setTitle("Out of time!");
		alertDialogBuilder.setMessage("Do you want to try this puzzle again?")
		.setCancelable(false)
		.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();
			}
		})
		.setNegativeButton("No", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();
			}
		});

		AlertDialog alertDialog = alertDialogBuilder.create();
		alertDialog.show();
	}

	/**Sets touch detection for imageViews*/
	public void setImageViewTouch(){
		//set touch for imageviews
		gestureDetector = new GestureDetector(mContext, new MyGestureDetector());
		onTouchListener = new View.OnTouchListener() {
			public boolean onTouch(View v, MotionEvent event) {
				return gestureDetector.onTouchEvent(event);
			}
		};

		image1.setOnTouchListener(onTouchListener);
		image2.setOnTouchListener(onTouchListener);
		image3.setOnTouchListener(onTouchListener);
		image4.setOnTouchListener(onTouchListener);
		image5.setOnTouchListener(onTouchListener);
		image6.setOnTouchListener(onTouchListener);
		image7.setOnTouchListener(onTouchListener);
		image8.setOnTouchListener(onTouchListener);
		image9.setOnTouchListener(onTouchListener);
	}

	/**Detects touch and starts timer*/
	class MyGestureDetector extends SimpleOnGestureListener {
		int timerCount = 0;
		@Override
		public boolean onDown(MotionEvent e) {
			count++;
			onTap(e);
			return false;
		}

	}

	/**Assigns bitmaps from the SD to image Views*/
	public void assignImageViewPics(){
		ImageView previewThumbnail = new ImageView(mContext);
		int[] ids= {R.id.imageView01,R.id.imageView02,R.id.imageView03,R.id.imageView04,R.id.imageView05,R.id.imageView06,R.id.imageView07,R.id.imageView08,R.id.imageView09};
		for(int i=0; i<9;i++){
			previewThumbnail=(ImageView)mView.findViewById(ids[i]);
			Bitmap bitmap;
			bitmap = chunkedImages.get(images.get(i)); //get image using images array that has been shuffled
			previewThumbnail.setImageBitmap(bitmap);
		}
	}

	/**Initializes the dimensions of the imageviews that hold the chunked 
	 * images*/
	public void imageViewsDimensions(){
		WindowManager wm = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
		Display display = wm.getDefaultDisplay();
		//Display  display = ((Activity) mContext).getWindowManager().getDefaultDisplay();
		//width of the imageViews
		//minus 6 to allows for space between imageViews
		int swidth = (int) (display.getWidth()/3-6);
		image1.getLayoutParams().width = swidth;
		image1.getLayoutParams().height = swidth;
		image2.getLayoutParams().width = swidth;
		image2.getLayoutParams().height = swidth;
		image3.getLayoutParams().width = swidth;
		image3.getLayoutParams().height = swidth;
		image4.getLayoutParams().width = swidth;
		image4.getLayoutParams().height = swidth;
		image5.getLayoutParams().width = swidth;
		image5.getLayoutParams().height = swidth;
		image6.getLayoutParams().width = swidth;
		image6.getLayoutParams().height = swidth;
		image7.getLayoutParams().width = swidth;
		image7.getLayoutParams().height = swidth;
		image8.getLayoutParams().width = swidth;
		image8.getLayoutParams().height = swidth;
		image9.getLayoutParams().width = swidth;
		image9.getLayoutParams().height = swidth;
	}


	/**Initialize imageViews to hold chucked images*/
	public void imageViewsHelper(){
		image1 = (ImageView) mView.findViewById(R.id.imageView01);
		image2 = (ImageView) mView.findViewById(R.id.imageView02);
		image3 = (ImageView) mView.findViewById(R.id.imageView03);
		image4 = (ImageView) mView.findViewById(R.id.imageView04);
		image5 = (ImageView) mView.findViewById(R.id.imageView05);
		image6 = (ImageView) mView.findViewById(R.id.imageView06);
		image7 = (ImageView) mView.findViewById(R.id.imageView07);
		image8 = (ImageView) mView.findViewById(R.id.imageView08);
		image9 = (ImageView) mView.findViewById(R.id.imageView09);
	}

	ImageView temp = null;
	int in=100, fin=100;
	/**Sets animation when user makes first touch*/
	public void setAnimation(ImageView im, int i){
		Animation animation = AnimationUtils.loadAnimation(mContext, animationToRun);
		im.startAnimation(animation );
		im.setPadding(8, 8,8,8);
		im.setBackgroundColor(Color.WHITE);
		temp=im;
		in=i;
	}

	public void secondTouch(ImageView imageView, int i){
		count=0;
		if(!(imageView.equals(temp))){
			switchCounter++;
		}
		temp.setPadding(0,0,0,0);
		temp.clearAnimation();
		fin=i;
		swapImages(temp,imageView,in,fin);
	}

	/**Helper to swap images on imageViews*/
	private void swapImages(ImageView initial, ImageView current, int init, int finImage){
		Bitmap bitmap;
		int tmp, tmp2;
		try {
			//swap the images in initial and current imageViews
			//bitmap = BitmapFactory.decodeStream(this.openFileInput("img"+images.get(in)));
			initial.setImageBitmap(BitmapFactory.decodeStream(mContext.openFileInput("img"+fin)));
			current.setImageBitmap(BitmapFactory.decodeStream(mContext.openFileInput("img"+in)));
			//swap the numbers in arrayList for in and fin
			tmp = init;
			tmp2 = images.indexOf(finImage);
			images.set(images.indexOf(init), finImage);
			images.set(tmp2, tmp);
			checkFinish();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

	}

	/**Checks whether the PicSwitch is solved*/
	private void checkFinish(){
		if(images.get(0)==0&&
				images.get(1)==1&&
				images.get(2)==2&&
				images.get(3)==3&&
				images.get(4)==4&&
				images.get(5)==5&&
				images.get(6)==6&&
				images.get(7)==7&&
				images.get(8)==8){
			done();
		}
	}

	/**Detects touches on imageViews 
	 * Shows the necessary animations
	 * Calls swap images to swap the images*/
	private void onTap(MotionEvent event){
		int firstRow = getLocation(image1)[1]+image1.getHeight(); //lower y coordinate of row 1

		//first column
		if(image1.getLeft()<event.getRawX() && image1.getLeft()+image1.getHeight()>event.getRawX()){
			//image1
			if(image1.getTop()<event.getRawY() && getLocation(image1)[1]+image1.getHeight()>event.getRawY()){
				if(count==1){
					setAnimation(image1, images.get(0));
				}
				else if(count==2){
					secondTouch(image1, images.get(0));
				}
			}
			else if(image4.getTop()<event.getRawY() && getLocation(image4)[1]+image4.getHeight()>event.getRawY()){
				if(count==1){
					setAnimation(image4, images.get(3));
				}
				else if(count==2){
					secondTouch(image4, images.get(3));
				}
			}
			else if(image7.getTop()<event.getRawY() && getLocation(image7)[1]+image7.getHeight()>event.getRawY()){
				if(count==1){
					setAnimation(image7, images.get(6));
				}
				else if(count==2){
					secondTouch(image7, images.get(6));
				}
			}

		}
		//second column
		if(image2.getLeft()<event.getRawX() && image2.getLeft()+image2.getHeight()>event.getRawX()){
			//image1
			if(image2.getTop()<event.getRawY() && firstRow>event.getRawY()){
				if(count==1){
					setAnimation(image2, images.get(1));
				}
				else if(count==2){
					secondTouch(image2, images.get(1));
				}
			}
			else if(image5.getTop()<event.getRawY() && getLocation(image5)[1]+image5.getHeight()>event.getRawY()){
				if(count==1){
					setAnimation(image5, images.get(4));
				}
				else if(count==2){
					secondTouch(image5, images.get(4));
				}
			}
			else if(image8.getTop()<event.getRawY() && getLocation(image8)[1]+image8.getHeight()>event.getRawY()){
				if(count==1){
					setAnimation(image8, images.get(7));
				}
				else if(count==2){
					secondTouch(image8, images.get(7));
				}
			}

		}
		//third column
		if(image3.getLeft()<event.getRawX() && image3.getLeft()+image3.getHeight()>event.getRawX()){
			//image1
			if(image3.getTop()<event.getRawY() && firstRow>event.getRawY()){
				if(count==1){
					setAnimation(image3, images.get(2));
				}
				else if(count==2){
					secondTouch(image3, images.get(2));
				}
			}
			else if(image6.getTop()<event.getRawY() && getLocation(image6)[1]+image6.getHeight()>event.getRawY()){
				if(count==1){
					setAnimation(image6, images.get(5));
				}
				else if(count==2){
					secondTouch(image6, images.get(5));
				}
			}
			else if(image9.getTop()<event.getRawY() && getLocation(image9)[1]+image9.getHeight()>event.getRawY()){
				if(count==1){
					setAnimation(image9, images.get(8));
				}
				else if(count==2){
					secondTouch(image9, images.get(8));
				}
			}

		}
	}

	private int[] getLocation(View mView) {
		Rect mRect = new Rect();
		int[] location = new int[2];

		mView.getLocationOnScreen(location);

		mRect.left = location[0];
		mRect.top = location[1];
		mRect.right = location[0] + mView.getWidth();
		mRect.bottom = location[1] + mView.getHeight();

		return location;
	}

	ArrayList<Bitmap> chunkedImages; //saves the chunked images

	/**method to split the image in imageView*/
	private void splitImage(Bitmap b, int chunkNumbers) {    
		Bitmap bitmap = b;

		//For the number of rows and columns of the grid to be displayed
		int rows,cols;

		//For height and width of the small image chunks 
		int chunkHeight,chunkWidth;

		//To store all the small image chunks in bitmap format in this list 
		chunkedImages = new ArrayList<Bitmap>(chunkNumbers);

		//Getting the scaled bitmap of the source image
		Bitmap scaledBitmap = Bitmap.createScaledBitmap(bitmap, bitmap.getWidth(), bitmap.getHeight(), true);

		rows = cols = (int) Math.sqrt(chunkNumbers);
		chunkHeight = bitmap.getHeight()/rows;
		chunkWidth = bitmap.getWidth()/cols;


		//xCoord and yCoord are the pixel positions of the image chunks
		int yCoord = 0;
		for(int x=0; x<rows; x++){
			int xCoord = 0;
			for(int y=0; y<cols; y++){
				chunkedImages.add(Bitmap.createBitmap(scaledBitmap, xCoord, yCoord, chunkWidth, chunkHeight));

				xCoord += chunkWidth;
			}
			yCoord += chunkHeight;
		}
		createImageHelper(chunkedImages);

	}

	/**Creates images from the bitmaps*/
	public String createImageFromBitmap(Bitmap bitmap, String name) {
		String fileName = name;//no .png or .jpg needed
		try {
			ByteArrayOutputStream bytes = new ByteArrayOutputStream();
			bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
			FileOutputStream fo = mContext.openFileOutput(fileName, Context.MODE_PRIVATE);
			fo.write(bytes.toByteArray());
			// remember close file output
			fo.close();
		} catch (Exception e) {
			e.printStackTrace();
			fileName = null;
		}
		return fileName;

	}

	/**Saves images to sd and starts next activity*/
	public void createImageHelper(ArrayList<Bitmap> bitmap){
		for(int i=0;i<9;i++){
			createImageFromBitmap(bitmap.get(i), "img"+i);
		}
		try {
			img0= BitmapFactory.decodeStream(mContext.openFileInput("img0"));
			img1= BitmapFactory.decodeStream(mContext.openFileInput("img1"));
			img2= BitmapFactory.decodeStream(mContext.openFileInput("img2"));
			img3= BitmapFactory.decodeStream(mContext.openFileInput("img3"));
			img4= BitmapFactory.decodeStream(mContext.openFileInput("img4"));
			img5= BitmapFactory.decodeStream(mContext.openFileInput("img5"));
			img6= BitmapFactory.decodeStream(mContext.openFileInput("img6"));
			img7= BitmapFactory.decodeStream(mContext.openFileInput("img7"));
			img8= BitmapFactory.decodeStream(mContext.openFileInput("img8"));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

}
