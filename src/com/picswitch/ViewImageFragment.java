package com.picswitch;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

/**
 * A simple {@link Fragment} subclass.
 *
 */
public class ViewImageFragment extends Fragment {
	
	public interface ViewFragmentListener {
		public void onNextClicked();
		public void onViewFragmentBackClicked();
	}
	
	private ViewFragmentListener viewFragListener;
	private Button backButton;

	public ViewImageFragment() {
		// Required empty public constructor
	}

	public static ViewImageFragment newInstance(Bundle configOptions){
		ViewImageFragment viewFrag = new ViewImageFragment();
		viewFrag.setArguments(configOptions);
		return viewFrag;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		View mView;
		mView = inflater.inflate(R.layout.view_image_fragment, container, false);
		
		backButton = (Button) mView.findViewById(R.id.pretend_back_button);
		backButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				viewFragListener.onNextClicked();
			}
		});
		
		return mView;
	}
	
	@Override
	public void onAttach(Activity activity){
		super.onAttach(activity);
		
		if(activity instanceof ViewFragmentListener){
			viewFragListener = (ViewFragmentListener) activity;
		}else{
			throw new IllegalArgumentException(
					"Activity must implement ViewFragment Listener");
		}
	}
	
	public void onBackPressed(){
		viewFragListener.onViewFragmentBackClicked();
	}

}
