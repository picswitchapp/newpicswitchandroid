package com.picswitch;

import com.parse.ParseFile;
import com.parse.ParseUser;
import com.squareup.picasso.Picasso;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

public class UploadProfilePicFragment extends Fragment {

	Context mContext;
	ImageView picture;

	public interface UploadProfilePicFragmentListener{
		public void onUploadProfilePicBackClicked();
	}

	private UploadProfilePicFragmentListener mProfPicListener;

	public UploadProfilePicFragment() {
	}

	public static UploadProfilePicFragment newInstance(Bundle configOptions){
		UploadProfilePicFragment picFrag = new UploadProfilePicFragment();
		Bundle args = new Bundle(configOptions);
		picFrag.setArguments(args);
		return picFrag;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View mView;
		mView = inflater.inflate(R.layout.upload_profile_pic_fragment, container, false);

		ImageView leftArrow = (ImageView) mView.findViewById(R.id.left_arrow_upload_profile_image);
		leftArrow.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				mProfPicListener.onUploadProfilePicBackClicked();
			}
		});

		picture = (ImageView) mView.findViewById(R.id.profile_image);

		//set the imageView to the one on the user's table entry
		ParseFile mFile = null;
		try{
			mFile = ParseUser.getCurrentUser().getParseFile(ParseConstants.KEY_PROFILE_PICTURE);
		}catch(Exception e){
			e.printStackTrace();
		}

		if(mFile != null){
			Uri pictureUri = Uri.parse(mFile.getUrl());
			Picasso.with(mContext)
			.load(pictureUri)
			.placeholder(R.drawable.picasso_loading_animation)
			.into(picture);
		}else{
			//put default image there
			picture.setImageDrawable(getResources().getDrawable(R.drawable.no_profile_image));
		}

		return mView;
	}

	@Override
	public void onAttach(Activity activity){
		super.onAttach(activity);
		if(activity instanceof UploadProfilePicFragmentListener){
			mProfPicListener = (UploadProfilePicFragmentListener) activity;
		}else{
			throw new IllegalArgumentException(
					"Activity must implement UploadProfilePicFragmentListener");
		}
	}

}
