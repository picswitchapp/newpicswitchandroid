package com.picswitch;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.graphics.Rect;
import android.hardware.Camera;
import android.hardware.Camera.Size;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.util.Log;
import android.view.Display;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

public class CameraActivity extends Activity {
	
	private Preview mPreview;
	Camera mCamera;
	int mNumberOfCameras;
	int mCurrentCamera;  // Camera ID currently chosen
	int mCameraCurrentlyLocked;  // Camera ID that's actually acquired
	
	FrameLayout mFrame;
	RelativeLayout cameraOverlay;
	
	//will hold the screen size
	Point displaySize = new Point();
	
	FocusCrosshair tapFocusIcon;
	
	private static final int FOCUS_AREA_SIZE = 300;
	private static final String TAG = "CameraActivityy";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.camera_activity);
		
		mPreview = new Preview(CameraActivity.this);
		mFrame = (FrameLayout) findViewById(R.id.camera_activity_preview);
		tapFocusIcon = (FocusCrosshair) findViewById(R.id.focus_crosshair);
		cameraOverlay = (RelativeLayout) findViewById(R.id.camera_activity_overlay);
		
		Display display = getWindowManager().getDefaultDisplay();
		display.getSize(displaySize);
		
		mPreview.setOnTouchListener(new OnTouchListener() {
			private GestureDetector gestureDetector = new GestureDetector(CameraActivity.this, new GestureDetector.SimpleOnGestureListener() {
				@Override
				public boolean onDoubleTapEvent(MotionEvent e) {
					//switch cameras

					//code for switching to front camera goes here

					//					if(mNumberOfCameras > 1){
					//						if (mCamera != null) {
					//							mCamera.stopPreview();
					//							mPreview.setCamera(null);
					//							mCamera.release();
					//							mCamera = null;
					//						}
					//
					//						// Acquire the next camera and request Preview to reconfigure
					//						// parameters.
					//						mCurrentCamera = (mCameraCurrentlyLocked + 1) % mNumberOfCameras;
					//						mCamera = Camera.open(mCurrentCamera);
					//						mCameraCurrentlyLocked = mCurrentCamera;
					//						mCamera.setDisplayOrientation(90);
					//						mPreview.switchCamera(mCamera);
					//
					//						// Start the preview
					//						mCamera.startPreview();
					//					}
					//					return super.onDoubleTap(e);
					return false;
				}

				@Override
				public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY){
					
					System.out.println("velocity X = " + velocityX);
					
					if(velocityX > 7500){
						//open camera activity
						Toast.makeText(getBaseContext(), "camera swipe", Toast.LENGTH_SHORT).show();
						CameraActivity.this.finish();
					}
					
					return false;
				}

				@Override
				public boolean onSingleTapConfirmed(MotionEvent e){

					//make visible the focus shape
					int boxSize = displaySize.x / 6;
					RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(boxSize,boxSize);
					params.leftMargin = Math.round(e.getRawX() - (boxSize / 2));
					params.topMargin = Math.round(e.getRawY() - (boxSize / 2));
					tapFocusIcon.showStart();
					cameraOverlay.removeView(tapFocusIcon);
					cameraOverlay.addView(tapFocusIcon, params);

					Animation mFocusAnimationIn  = AnimationUtils.loadAnimation(CameraActivity.this, R.anim.focus_animation_in);
					final Animation mFocusAnimationOut = AnimationUtils.loadAnimation(CameraActivity.this, R.anim.focus_animation_out);
					tapFocusIcon.startAnimation(mFocusAnimationIn);
					mFocusAnimationIn.setAnimationListener(new AnimationListener() {
						@Override
						public void onAnimationStart(Animation animation) {}
						@Override
						public void onAnimationEnd(Animation animation) {
							tapFocusIcon.setAnimation(null);
							tapFocusIcon.setAnimation(mFocusAnimationOut);
						}
						@Override
						public void onAnimationRepeat(Animation animation) {}
					});
					mFocusAnimationOut.setAnimationListener(new AnimationListener() {
						@Override
						public void onAnimationStart(Animation animation) {}
						@Override
						public void onAnimationEnd(Animation animation) {
							tapFocusIcon.clear();
						}
						@Override
						public void onAnimationRepeat(Animation animation) {}
					});


					focusOnTouch(e);

					//System.out.println("touch at: " + e.getRawX() + ", " + e.getRawY());
					//System.out.println("parentView starts at " + pixelsTopScreenToCameraCutOut);

					return false;
				}

			});

			@Override
			public boolean onTouch(View v, MotionEvent event) {

				gestureDetector.onTouchEvent(event);

				return true;
			}
		});
	}
	
	private void focusOnTouch(MotionEvent event) {
		if (mCamera != null ) {
			mCamera.cancelAutoFocus();
			Camera.Parameters parameters = mCamera.getParameters();
			if (parameters.getMaxNumMeteringAreas() > 0){
				Log.i(TAG,"fancy !");
				Rect rect = calculateFocusArea(event.getX(), event.getY());

				System.out.println("number of focus areas: " + parameters.getMaxNumFocusAreas());

				parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
				List<Camera.Area> meteringAreas = new ArrayList<Camera.Area>();
				meteringAreas.clear();
				meteringAreas.add(new Camera.Area(rect, 800));
				parameters.setFocusAreas(meteringAreas);

				try{
					mCamera.setParameters(parameters);
				}catch(Exception e){
					e.printStackTrace();
				}

				mCamera.autoFocus(mAutoFocusTakePictureCallback);
			}else {
				mCamera.autoFocus(mAutoFocusTakePictureCallback);
			}
		}
	}
	
	private Camera.AutoFocusCallback mAutoFocusTakePictureCallback = new Camera.AutoFocusCallback() {
		@Override
		public void onAutoFocus(boolean success, Camera camera) {
			if (success) {
				// do something...
				Log.i("tap_to_focus","success!");
			} else {
				// do something...
				Log.i("tap_to_focus","fail!");
			}
		}
	};
	
	private Rect calculateFocusArea(float x, float y) {
		int left = clamp(Float.valueOf((x / mPreview.getWidth()) * 2000 - 1000).intValue(), FOCUS_AREA_SIZE);
		int top = clamp(Float.valueOf((y / mPreview.getHeight()) * 2000 - 1000).intValue(), FOCUS_AREA_SIZE);

		return new Rect(left, top, left + FOCUS_AREA_SIZE, top + FOCUS_AREA_SIZE);
	}
	
	private int clamp(int touchCoordinateInCameraReper, int focusAreaSize) {
		int result;
		if (Math.abs(touchCoordinateInCameraReper)+focusAreaSize/2>1000){
			if (touchCoordinateInCameraReper>0){
				result = 1000 - focusAreaSize/2;
			} else {
				result = -1000 + focusAreaSize/2;
			}
		} else{
			result = touchCoordinateInCameraReper - focusAreaSize/2;
		}
		return result;
	}
	
	@Override
	public void onResume(){
		super.onResume();
		
		// Use mCurrentCamera to select the camera desired to safely restore
		// the fragment after the camera has been changed

		System.out.println("onResume called");

		if(mCamera == null){
			newOpenCamera(mCurrentCamera);
		}
	}
	
	@Override
	public void onPause(){
		super.onPause();
		System.out.println("onPause called");

		// Because the Camera object is a shared resource, it's very
		// important to release it when the activity is paused.
		if (mCamera != null) {
			mCamera.stopPreview();
			mPreview.setCamera(null);
			mCamera.setPreviewCallback(null);
			mCamera.release();
			mCamera = null;
		}
		mFrame.removeAllViews();

		if (mThread != null && mThread.isAlive()){
			mThread.interrupt();
		}
	}
	
	private void oldOpenCamera(int currentCamera){
		try{
			mCamera = Camera.open(currentCamera);
		}catch(Exception e){
			e.printStackTrace();
			System.out.println("oldOpenCamera exception");
		}
	}

	private void newOpenCamera(int currentCamera){
		if(mThread == null){
			mThread = new CameraHandlerThread();
		}

		synchronized (mThread) {
			mThread.openCamera(currentCamera);
		}
	}

	private CameraHandlerThread mThread = null;
	private class CameraHandlerThread extends HandlerThread {
		Handler mHandler = null;

		CameraHandlerThread(){
			super("CameraHandlerThread");
			start();
			mHandler = new Handler(getLooper());
		}

		synchronized void notifyCameraOpened(){
			notify();
		}

		void openCamera(final int currentCamera){
			mHandler.post(new Runnable() {

				@Override
				public void run() {
					oldOpenCamera(currentCamera);
					notifyCameraOpened();

					runOnUiThread(new Runnable(){

						@Override
						public void run() {
							if(mCamera != null){
								mCameraCurrentlyLocked = mCurrentCamera;
								mCamera.setDisplayOrientation(90);
								mPreview.setCamera(mCamera);
								if(mFrame.getChildCount() > 0){
									mFrame.removeAllViews();
								}
								mFrame.addView(mPreview);
								mCamera.startPreview();
							}else{
								//opening the camera failed, so try again
								Toast.makeText(CameraActivity.this, "Could not open camera", Toast.LENGTH_SHORT).show();
								//newOpenCamera(currentCamera);
							}
						}
					});
				}
			});
		}
	}
	
	/**
	 * A simple wrapper around a Camera and a SurfaceView that renders a centered
	 * preview of the Camera to the surface. We need to center the SurfaceView
	 * because not all devices have cameras that support preview sizes at the same
	 * aspect ratio as the device's display.
	 */
	public class Preview extends ViewGroup implements SurfaceHolder.Callback {

		//	private static final String FOCUS_MODE_CONTINUOUS_PICTURE = "continuous-picture";

		private final String TAG = "Preview";

		SurfaceView mSurfaceView;
		SurfaceHolder mHolder;
		Size mPreviewSize;
		List<Size> mSupportedPreviewSizes;
		Camera mCamera;
		boolean mSurfaceCreated = false;

		int width,height;

		//	public boolean meteringAreaSupported;

		double displayAspectRatio;

		Context mContext;
		ImageView switchCamera;

		Point displaySize = new Point();

		public int getLocalWidth(){
			return width;
		}
		public int getLocalHeight(){
			return height;
		}

		Preview(Context context) {
			super(context);

			mContext = context;

			mSurfaceView = new SurfaceView(context);

			addView(mSurfaceView);

			// Install a SurfaceHolder.Callback so we get notified when the
			// underlying surface is created and destroyed.
			mHolder = mSurfaceView.getHolder();
			mHolder.addCallback(this);
			mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

			Display display = ((Activity)context).getWindowManager().getDefaultDisplay();
			display.getSize(displaySize);
		}

		public void setCamera(Camera camera) {
			mCamera = camera;
			if (mCamera != null) {
				mSupportedPreviewSizes = mCamera.getParameters().getSupportedPreviewSizes();
				if (mSurfaceCreated){
					requestLayout();
				}
			}
		}

		public Camera getCamera(){
			return mCamera;
		}

		public void switchCamera(Camera camera) {
			setCamera(camera);
			try {
				camera.setPreviewDisplay(mHolder);
			} catch (IOException exception) {
				Log.e(TAG, "IOException caused by setPreviewDisplay()", exception);
			}
		}

		Point screenSize = new Point();

		@Override
		protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
			// We purposely disregard child measurements because act as a
			// wrapper to a SurfaceView that centers the camera preview instead
			// of stretching it.

			width = resolveSize(getSuggestedMinimumWidth(),
					widthMeasureSpec);
			height = resolveSize(getSuggestedMinimumHeight(),
					heightMeasureSpec);

			setMeasuredDimension(width, height);

			if (mSupportedPreviewSizes != null) {
				mPreviewSize = getOptimalPreviewSize(mSupportedPreviewSizes, width, height);
			}

			if (mCamera != null) {
				Camera.Parameters parameters = mCamera.getParameters();
				parameters.setPreviewSize(mPreviewSize.width, mPreviewSize.height);

				System.out.println("mPreviewSize.width: "  + mPreviewSize.width);
				System.out.println("mPreviewSize.height: " + mPreviewSize.height);

				try{
					mCamera.setParameters(parameters);
				}catch(Exception e){
					e.printStackTrace();
				}
			}
		}

		@Override
		protected void onLayout(boolean changed, int l, int t, int r, int b) {
			if (getChildCount() > 0) {
				final View child = getChildAt(0);

				Display display = ((Activity) mContext).getWindowManager().getDefaultDisplay();
				display.getSize(screenSize);
				int width = screenSize.x;
				int height = screenSize.y;

				int previewWidth = width;
				int previewHeight = height;
				if (mPreviewSize != null) {
					previewWidth = mPreviewSize.height;
					previewHeight = mPreviewSize.width;
					System.out.println("inside onLayout...mPreviewSize.width = " + mPreviewSize.width);
					System.out.println("inside onLayout...mPreviewSize.height = " + mPreviewSize.height);
				}

				// Center the child SurfaceView within the parent.
				if (width * previewHeight < height * previewWidth) {
					final int scaledChildWidth = previewWidth * height / previewHeight;
					child.layout((width - scaledChildWidth) / 2, 0, (width + scaledChildWidth) / 2, height);
				} else {
					final int scaledChildHeight = previewHeight * width / previewWidth;
					child.layout(0, (height - scaledChildHeight) / 2, width, (height + scaledChildHeight) / 2);
				}
			}
		}

		public void surfaceCreated(SurfaceHolder holder) {
			// The Surface has been created, acquire the camera and tell it where
			// to draw.
			try {
				if (mCamera != null) {

					mCamera.setPreviewDisplay(holder);

				}
			} catch (IOException exception) {
				Log.e(TAG, "IOException caused by setPreviewDisplay()", exception);
			}
			if (mPreviewSize == null) requestLayout();
			mSurfaceCreated = true;
		}

		public void surfaceDestroyed(SurfaceHolder holder) {
			// Surface will be destroyed when we return, so stop the preview.
		}

		private Size getOptimalPreviewSize(List<Size> sizes, int w, int h) {
			final double ASPECT_TOLERANCE = 0.05;
			//		double targetRatio = (double) w / h;
			double targetRatio = (double) 1.0;
			//		double targetRatio = displayAspectRatio;
			System.out.println("target ratio = " + targetRatio);
			if (sizes == null)
				return null;

			Size optimalSize = null;

			double minDiff = Double.MAX_VALUE;

			int targetHeight = h;

			System.out.println("targetHeight = " + targetHeight);

			// Try to find an size match aspect ratio and size
			for (Size size : sizes) {
				double ratio = (double) size.width / size.height;
				if (Math.abs(ratio - targetRatio) > ASPECT_TOLERANCE)
					continue;
				if (Math.abs(size.height - targetHeight) < minDiff) {
					optimalSize = size;
					minDiff = Math.abs(size.height - targetHeight);
				}
			}

			// Cannot find the one match the aspect ratio, ignore the requirement
			if (optimalSize == null) {
				minDiff = Double.MAX_VALUE;
				for (Size size : sizes) {
					if (Math.abs(size.height - targetHeight) < minDiff) {
						optimalSize = size;
						minDiff = Math.abs(size.height - targetHeight);
					}
				}
			}
			System.out.println("optimal size: " + optimalSize.width + " x " + optimalSize.height);
			if(optimalSize.width < optimalSize.height){
				int temp = optimalSize.width;
				optimalSize.width = optimalSize.height;
				optimalSize.height = temp;
			}

			//			if((optimalSize.height > displaySize.y) || (optimalSize.width > displaySize.x)){
			//				//optimal size selected is larger than the screen, so recall this function after removeing this size.
			//				sizes.remove(optimalSize);
			//				getOptimalPreviewSize(sizes, w, h);
			//			}

			System.out.println("optimal size selected: " + optimalSize.width + " x " + optimalSize.height);

			return optimalSize;

		}

		public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
			// Now that the size is known, set up the camera parameters and begin
			// the preview.

			System.out.println("surfaceChanged called");

			if(mCamera != null){
				Camera.Parameters parameters = mCamera.getParameters();

				mPreviewSize = getOptimalPreviewSize(parameters.getSupportedPreviewSizes(), width, height);

				List<String> focusModes = parameters.getSupportedFocusModes();
				if (focusModes.contains(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE)) {
					parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
					System.out.println("continuous autofocus is supported");
				}else{
					parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
				}

				System.out.println("surfaceChanged preview sizes set as: " + mPreviewSize.width + " x " + mPreviewSize.height);

				parameters.setPreviewSize(mPreviewSize.width, mPreviewSize.height);
				parameters.setWhiteBalance(Camera.Parameters.WHITE_BALANCE_AUTO);

				requestLayout();

				mCamera.setParameters(parameters);
				mCamera.setDisplayOrientation(90);

				mCamera.startPreview();

			}
		}
	}

}
