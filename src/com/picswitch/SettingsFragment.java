package com.picswitch;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class SettingsFragment extends Fragment {
	
	public interface SettingsFragmentListener{
		public void onSettingsBackClicked();
		public void onChangeNameClicked();
		public void onChangePasswordClicked();
		public void onSetPhoneNumberClicked();
		public void onSetUsernameClicked();
		public void onUploadProfileImageClicked();
		public void onFaqsClicked();
		public void onTermsClicked();
		public void onPrivacyPolicyClicked();
		public void onContestRulesClicked();
		public void onLogOutClicked();
	}
	
	private SettingsFragmentListener mSettingsListener;

	public SettingsFragment() {
	}
	
	public static SettingsFragment newInstance(Bundle configOptions){
		SettingsFragment settingsFrag = new SettingsFragment();
		Bundle args = new Bundle(configOptions);
		settingsFrag.setArguments(args);
		return settingsFrag;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View mView;
		mView = inflater.inflate(R.layout.settings_fragment, container, false);
		
		//setting all of these click listeners
		//back arrow
		ImageView backArrow = (ImageView) mView.findViewById(R.id.left_arrow_settings);
		backArrow.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				mSettingsListener.onSettingsBackClicked();
			}
		});
		//change name
		TextView changeNameButton = (TextView) mView.findViewById(R.id.change_name);
		changeNameButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				mSettingsListener.onChangeNameClicked();
			}
		});
		//change password
		TextView changePassButton = (TextView) mView.findViewById(R.id.change_password);
		changePassButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				mSettingsListener.onChangePasswordClicked();
			}
		});
		
		//set phone number
		TextView setPhone = (TextView) mView.findViewById(R.id.enter_phone_number);
		setPhone.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				mSettingsListener.onSetPhoneNumberClicked();
			}
		});
		
		//set username
		TextView setUsernameButton = (TextView) mView.findViewById(R.id.set_username);
		setUsernameButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				mSettingsListener.onSetUsernameClicked();
			}
		});
		
		//upload profile picture
		TextView uploadPicButton = (TextView) mView.findViewById(R.id.upload_profile_image);
		uploadPicButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				mSettingsListener.onUploadProfileImageClicked();
			}
		});
		
		//FAQs
		TextView faqsButton = (TextView) mView.findViewById(R.id.settings_FAQs);
		faqsButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				mSettingsListener.onFaqsClicked();
			}
		});
		
		//Terms of Service
		TextView termsButton = (TextView) mView.findViewById(R.id.settings_terms_of_service);
		termsButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				mSettingsListener.onTermsClicked();
			}
		});
		
		//privacy policy
		TextView privacyPolicy = (TextView) mView.findViewById(R.id.settings_privacy_policy);
		privacyPolicy.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				mSettingsListener.onPrivacyPolicyClicked();
			}
		});
		
		//contest rules
		TextView contestRules = (TextView) mView.findViewById(R.id.settings_contest_rules);
		contestRules.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				mSettingsListener.onContestRulesClicked();
			}
		});
		
		//logout
		TextView logoutButton = (TextView) mView.findViewById(R.id.log_out);
		logoutButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				mSettingsListener.onLogOutClicked();
			}
		});
		
		
		return mView;
	}
	
	@Override
	public void onAttach(Activity activity){
		super.onAttach(activity);
		
		if(activity instanceof SettingsFragmentListener){
			mSettingsListener = (SettingsFragmentListener) activity;
		}else{
			throw new IllegalArgumentException(
					"Activity must implement SettingsFragmentListener");
		}
	}
}
