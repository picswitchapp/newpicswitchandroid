package com.picswitch;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.Toast;

/**
 * A simple {@link Fragment} subclass.
 *
 */
public class MainPageFragment extends Fragment {

	private Context mContext;
	
	Calendar timeTaken = Calendar.getInstance(TimeZone.getTimeZone("GMT"));

	private ExpandableListView mListView;

	public String solvedFeaturedFromParse;

	List<ParseObject> featuredParseObjects;
	List<ParseObject> newParseObjects;

	public boolean newDone = false;
	public boolean featuredDone = false;
	
	View mView;

	public MainPageFragment() {
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		mContext = container.getContext();

		mView = inflater.inflate(R.layout.main_page_fragment, container, false);

		mListView = (ExpandableListView) mView.findViewById(R.id.main_page_list_view);
		mListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
			
			@Override
			public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
				Toast.makeText(mContext, "child click", Toast.LENGTH_SHORT).show();
				
				//new messages
				if(groupPosition == 0){
					Intent intent = new Intent(mContext, ImageActivity.class);
					ParseObject newMessage = newParseObjects.get(childPosition);					
					
					ParseFile mFile = newMessage.getParseFile(ParseConstants.KEY_PHOTO);
					Uri messageUri = Uri.parse(mFile.getUrl());
					intent.setData(messageUri);
					
					String firstName="";
					String lastName="";

					try {
						firstName = newMessage.getParseUser(ParseConstants.KEY_SENDER).fetchIfNeeded().getString(ParseConstants.KEY_FIRST_NAME);
						lastName  = newMessage.getParseUser(ParseConstants.KEY_SENDER).fetchIfNeeded().getString(ParseConstants.KEY_LAST_NAME);
					} catch (ParseException e3) {
						e3.printStackTrace();
					}

					String lastNameInitial = ""; 
					if(lastName.length() > 0){
						lastNameInitial = lastName.substring(0, 1);
					}
					String titleName = firstName + " " + lastNameInitial + ".";
					intent.putExtra("titleName", titleName);
					
					String firstCaption = newMessage.getString(ParseConstants.CAPTION_FIRST);
					intent.putExtra(ParseConstants.CAPTION_FIRST, firstCaption);
					
					String messageID = newMessage.getObjectId();
					intent.putExtra(ParseConstants.KEY_OBJECT_ID, messageID);
					
					intent.putExtra(ParseConstants.KEY_MESSAGE_TYPE, ParseConstants.NEW_MESSAGE);
					
					startActivity(intent);
				}
				//featured messages
				else if(groupPosition == 1){
					Intent intent = new Intent(mContext, ImageActivity.class);
					ParseObject featuredMessage = featuredParseObjects.get(childPosition);
					
					ParseFile mFile = featuredMessage.getParseFile(ParseConstants.KEY_PHOTO);
					Uri messageUri = Uri.parse(mFile.getUrl());
					intent.setData(messageUri);
					
					String titleName = featuredMessage.getString(ParseConstants.KEY_COMPANY_NAME);
					intent.putExtra("titleName", titleName);
					
					String firstCaption = featuredMessage.getString(ParseConstants.CAPTION_FIRST);
					intent.putExtra(ParseConstants.CAPTION_FIRST, firstCaption);
					
					String messageID = featuredMessage.getObjectId();
					intent.putExtra(ParseConstants.KEY_OBJECT_ID, messageID);
					
					intent.putExtra(ParseConstants.KEY_MESSAGE_TYPE, ParseConstants.FEATURED_MESSAGE);
					
					startActivity(intent);
				}
				
				//else if for solved messages
				
				return true;
			}
		});
		
		new FindNewPuzzles().execute();
		new FindFeaturedPuzzles().execute();

		return mView;
	}
	
	@Override
	public void onResume(){
		super.onResume();
		timeTaken = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
	}

	public void setAdapter(){
		if(newDone && featuredDone){
			mView.findViewById(R.id.main_page_loading).setVisibility(View.GONE);
			List<String> headerNames = new ArrayList<String>();
			headerNames.add("new");
			headerNames.add("featured");
			Map<String, List<ParseObject>> map = new HashMap<String, List<ParseObject>>();
			map.put("new", newParseObjects);
			map.put("featured", featuredParseObjects);
			mListView.setAdapter(new MainPageExpandableAdapter(mContext, map, headerNames));
			mListView.expandGroup(0);
			mListView.expandGroup(1);
			mListView.setGroupIndicator(null);
		}
	}

	public class FindNewPuzzles extends AsyncTask<Void, Boolean, Void>{

		@Override
		protected Void doInBackground(Void... params) {
			ParseQuery<ParseObject> newQuery = new ParseQuery<ParseObject>(ParseConstants.CLASS_MESSAGES);
			newQuery.whereEqualTo(ParseConstants.KEY_RECIPIENTS, ParseUser.getCurrentUser());
			newQuery.addDescendingOrder(ParseConstants.KEY_CREATED_AT);
			newQuery.findInBackground(new FindCallback<ParseObject>() {

				@Override
				public void done(List<ParseObject> newMessages, ParseException e) {
					if(e == null){
						System.out.println("newMessage length: " + newMessages.size());
						newParseObjects = newMessages;
						publishProgress(true);
					}else{
						e.printStackTrace();
						publishProgress(false);
					}
				}
			});

			return null;
		}

		@Override
		protected void onProgressUpdate(Boolean... params){
			if(params[0]){
				newDone = true;
				setAdapter();
			}else{
				Toast.makeText(mContext, "Could not load new puzzles", Toast.LENGTH_SHORT).show();
			}
		}

	}

	public class FindFeaturedPuzzles extends AsyncTask<Void, Boolean, Void>{
		
		public void createSolvedString(){

			List temp;
			temp = ParseUser.getCurrentUser().getList(ParseConstants.KEY_FEATURED_SOLVED);
			String tmp = "";
			if(temp==null){
				solvedFeaturedFromParse = "";
			}else{
				solvedFeaturedFromParse = temp.toString();
			}

			if(solvedFeaturedFromParse.length() < 4){
				solvedFeaturedFromParse = "";
			}else{
				if(solvedFeaturedFromParse.contains("[")){
					tmp = solvedFeaturedFromParse.replace("[", "");
					solvedFeaturedFromParse = tmp;
					System.out.println("before left brace case");
				}
				if(solvedFeaturedFromParse.contains("]")){
					tmp = solvedFeaturedFromParse.replace("]", "");
					System.out.println("before right brace case");
					solvedFeaturedFromParse = tmp;
				}
			}
			System.out.println("before pass 2: " + solvedFeaturedFromParse);

		}

		@Override
		protected Void doInBackground(Void... params) {
			
			createSolvedString();
			String t = solvedFeaturedFromParse;
			if(t.contains(" ")){
				t = t.replaceAll(" ", "");
			}
			if(t.contains(" ")){
				t = t.replaceAll(" ", "");
			}
			List<String> tmpFeatured = Arrays.asList(t.split(","));
			
			Date currentDate = timeTaken.getTime();
			
			ParseQuery<ParseObject> featuredQuery = new ParseQuery<ParseObject>(ParseConstants.CLASS_FEATURED_PUZZLES);
			featuredQuery.whereNotContainedIn(ParseConstants.KEY_OBJECT_ID, tmpFeatured);
			featuredQuery.whereGreaterThanOrEqualTo("endDate", currentDate);
			featuredQuery.whereLessThanOrEqualTo("startDate", currentDate);
			featuredQuery.findInBackground(new FindCallback<ParseObject>() {

				@Override
				public void done(List<ParseObject> featuredMessages, ParseException e) {
					if(e == null){
						System.out.println("featuredMessages length: " + featuredMessages.size());
						featuredParseObjects = featuredMessages;
						publishProgress(true);
					}else{
						e.printStackTrace();
						publishProgress(false);
					}
				}
			});

			return null;
		}

		@Override
		protected void onProgressUpdate(Boolean... params){
			if(params[0]){
				featuredDone = true;
				setAdapter();
			}else{
				Toast.makeText(mContext, "Could not load featured puzzles", Toast.LENGTH_SHORT).show();
			}
		}

	}

}
