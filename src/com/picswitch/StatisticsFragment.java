package com.picswitch;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class StatisticsFragment extends Fragment{
	
	public interface StatisticsFragmentListener{
		public void onStatisticsBackClicked();
	}

	private StatisticsFragmentListener mStatsListener;
	
	public StatisticsFragment() {
	}
	
	public static StatisticsFragment newInstance(Bundle configOptions){
		StatisticsFragment statsFrag = new StatisticsFragment();
		Bundle args = new Bundle(configOptions);
		statsFrag.setArguments(args);
		return statsFrag;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View mView;
		mView = inflater.inflate(R.layout.statistics_fragment, container, false);
		
		return mView;
	}
	
	@Override 
	public void onAttach(Activity activity){
		super.onAttach(activity);
		
		if(activity instanceof StatisticsFragmentListener){
			mStatsListener = (StatisticsFragmentListener) activity;
		}else{
			throw new IllegalArgumentException(
					"Activity must implement StatisticsFragmentListener");
		}
	}

}
