package com.picswitch;

import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.parse.RefreshCallback;
import com.parse.ui.ParseLoginBuilder;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

/** 
 * Class which handles the main pages.  Will contain a view pager with two 
 * fragments, one for additional puzzles, one for the new page.
 * 
 * @author Joey Menzenski
 * */
public class MainActivity extends FragmentActivity {

	private static final int LOGIN_REQUEST = 0;

	private ViewPager mPager;
	private MainPagerAdapter mPagerAdapter;

	private TextView titleText;
	public  LinearLayout menuContainer;
	public  TextView upArrow;
	public  TextView downArrow;
	private TextView menuAddFriends;
	private TextView menuFriends;
	private TextView menuSettings;
	private TextView menuStatistics;

	private Typeface montserrat;
	private Typeface varella;

	private MotionEvent event1 = null;

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		handleLogIn();

		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
			Window window = getWindow();
			window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
			window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
			window.setStatusBarColor(Color.rgb(32, 99, 99));
		}

		montserrat = Typeface.createFromAsset(getAssets(), "Montserrat-Regular.ttf");
		varella = Typeface.createFromAsset(getAssets(), "VarelaRound.ttf");

		menuContainer = (LinearLayout) findViewById(R.id.main_menu_container);
		upArrow = (TextView) findViewById(R.id.up_triangle);
		downArrow = (TextView) findViewById(R.id.down_triangle);
		menuAddFriends = (TextView) findViewById(R.id.main_menu_add_friends_text);
		menuFriends = (TextView) findViewById(R.id.main_menu_friends_text);
		menuSettings = (TextView) findViewById(R.id.main_menu_settings_text);
		menuStatistics = (TextView) findViewById(R.id.main_menu_statistics_text);

		titleText = (TextView) findViewById(R.id.main_title_text);
		titleText.setTypeface(montserrat);

		setUpMainViewPager();
		setMenuFonts();
		buttonHelper();
	}

	@Override
	public void onBackPressed(){
		moveTaskToBack(false);
	}

	@Override
	public void onResume(){
		super.onResume();

		//close the menu if it is open
		if(menuContainer.getVisibility() == View.VISIBLE){
			menuContainer.setVisibility(View.GONE);
			upArrow.setVisibility(View.GONE);
			downArrow.setVisibility(View.VISIBLE);
		}

		if(ParseUser.getCurrentUser() != null){
			ParseUser.getCurrentUser().refreshInBackground(new RefreshCallback() {
				@Override
				public void done(ParseObject arg0, ParseException arg1) {
					System.out.println("user refreshed main activity");
				}
			});
		}
	}

	@Override
	public void onPause(){
		super.onPause();

	}

	/** 
	 * Helper function which determines if a user is currently logged in.  If no user is
	 * logged in, this function starts the log in activity.
	 * */
	public void handleLogIn(){
		if(ParseUser.getCurrentUser() == null){
			//open ParseLogInActivity
			ParseLoginBuilder loginBuilder = new ParseLoginBuilder(MainActivity.this);
			startActivityForResult(loginBuilder.build(), LOGIN_REQUEST);
		}else{
			return;
		}
	}

	/** 
	 * Helper function which handles the set up of the ViewPager for MainActivity. 
	 * */
	public void setUpMainViewPager(){
		mPager = (ViewPager) findViewById(R.id.main_pager);
		mPagerAdapter = new MainPagerAdapter(getSupportFragmentManager());

		mPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener(){

			@Override
			public void onPageScrollStateChanged(int arg0) {}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {}

			@Override 
			public void onPageSelected(int arg0) {
				mPager.setCurrentItem(arg0);
				if(arg0 == 0){
					titleText.setText("additional puzzles");
				}else{
					titleText.setText("picswitch");
				}
			}
		});

		mPager.setOnTouchListener(new OnTouchListener() {

			private GestureDetector newGestureDetector = new GestureDetector(getBaseContext(), new GestureDetector.SimpleOnGestureListener() {

				@Override
				public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
					//Toast.makeText(getBaseContext(), "on fling", Toast.LENGTH_SHORT).show();
					System.out.println("velocity X = " + velocityX);

					if(velocityX < -7500){
						//open camera activity
						Toast.makeText(getBaseContext(), "camera swipe", Toast.LENGTH_SHORT).show();
						Intent intent = new Intent(getBaseContext(), CameraActivity.class);
						startActivity(intent);
					}

					return false;
				}

				@Override
				public boolean onDown(MotionEvent e){
					return true;
				}

				@Override
				public boolean onSingleTapConfirmed(MotionEvent e){
					return false;
				}

				@Override
				public void onLongPress(MotionEvent e){

				}

				@Override
				public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY){
					return false;
				}

			});

			@Override
			public boolean onTouch(View v, MotionEvent event) {

				//close the menu if it is open
				if(menuContainer.getVisibility() == View.VISIBLE){
					menuContainer.setVisibility(View.GONE);
					upArrow.setVisibility(View.GONE);
					downArrow.setVisibility(View.VISIBLE);
				}

				//interpret touch event
				if(mPager.getCurrentItem() == 1){
					return newGestureDetector.onTouchEvent(event);
				}else{
					mPager.onTouchEvent(event);
				}
				return true;
			}
		});

		mPager.setAdapter(mPagerAdapter);
		mPager.setCurrentItem(1);
	}

	/**
	 * Helper function which sets the fonts for all of the menu TextViews
	 */
	public void setMenuFonts(){
		menuAddFriends.setTypeface(varella);
		menuFriends.setTypeface(varella);
		menuSettings.setTypeface(varella);
		menuStatistics.setTypeface(varella);
	}

	/**
	 * Function to handle menu clicks, opening the correct activity accordingly
	 * @param v
	 */
	public void menuClick(View v){
		Intent menuIntent = new Intent(MainActivity.this, MenuActivity.class);
		switch(v.getId()){
		case R.id.main_menu_add_friends:{
			Toast.makeText(getBaseContext(), "1", Toast.LENGTH_SHORT).show();
			menuIntent.putExtra(ParseConstants.KEY_MENU_FIRST_PAGE, ParseConstants.PAGE_ADD_FRIENDS);
			startActivity(menuIntent);
			break;
		}
		case R.id.main_menu_friends:{
			Toast.makeText(getBaseContext(), "2", Toast.LENGTH_SHORT).show();
			menuIntent.putExtra(ParseConstants.KEY_MENU_FIRST_PAGE, ParseConstants.PAGE_FRIENDS);
			startActivity(menuIntent);
			break;
		}
		case R.id.main_menu_settings:{
			Toast.makeText(getBaseContext(), "3", Toast.LENGTH_SHORT).show();
			menuIntent.putExtra(ParseConstants.KEY_MENU_FIRST_PAGE, ParseConstants.PAGE_SETTINGS);
			startActivity(menuIntent);
			break;
		}
		case R.id.main_menu_statistics:{
			Toast.makeText(getBaseContext(), "4", Toast.LENGTH_SHORT).show();
			menuIntent.putExtra(ParseConstants.KEY_MENU_FIRST_PAGE, ParseConstants.PAGE_STATS);
			startActivity(menuIntent);
			break;
		}
		default:{
			break;
		}
		}
	}

	/**
	 * Helper function to define click listeners for all buttons
	 */
	public void buttonHelper(){
		titleText.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				//handle the menu changes
				if(menuContainer.getVisibility() == View.VISIBLE){
					menuContainer.setVisibility(View.GONE);
					upArrow.setVisibility(View.GONE);
					downArrow.setVisibility(View.VISIBLE);
				}else{
					menuContainer.setVisibility(View.VISIBLE);
					upArrow.setVisibility(View.VISIBLE);
					downArrow.setVisibility(View.GONE);
				}
			}
		});

		ImageView menuButton = (ImageView) findViewById(R.id.main_menu_icon);
		menuButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				//handle the menu changes
				if(menuContainer.getVisibility() == View.VISIBLE){
					menuContainer.setVisibility(View.GONE);
					upArrow.setVisibility(View.GONE);
					downArrow.setVisibility(View.VISIBLE);
				}else{
					menuContainer.setVisibility(View.VISIBLE);
					upArrow.setVisibility(View.VISIBLE);
					downArrow.setVisibility(View.GONE);
				}
			}
		});
	}

}
