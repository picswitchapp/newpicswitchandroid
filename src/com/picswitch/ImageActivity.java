package com.picswitch;

import com.picswitch.SolveImageFragment.SolveFragmentListener;
import com.picswitch.ViewImageFragment.ViewFragmentListener;

import android.annotation.SuppressLint;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.Window;
import android.view.WindowManager;

public class ImageActivity extends FragmentActivity implements SolveFragmentListener, ViewFragmentListener {
	
	public static final String LOG_TAG = "ImageActivity";

	// All UI fragment transactions will happen within this parent layout element.
	private final int fragmentContainer = android.R.id.content;
	
	private Bundle configOptions;
	
	private boolean destroyed = false;
	private Uri messageUri;
	private String objectID;
	private String titleName;
	private String firstCaption;
	private String messageType;
	
	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
			Window window = getWindow();
			window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
			window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
			window.setStatusBarColor(Color.rgb(32, 99, 99));
		}
		
		configOptions = getMergedOptions();
		
		messageUri = getIntent().getData();
		String mUriString = messageUri.toString();
		titleName = getIntent().getExtras().getString("titleName");
		firstCaption = getIntent().getExtras().getString(ParseConstants.CAPTION_FIRST);
		objectID = getIntent().getExtras().getString(ParseConstants.KEY_OBJECT_ID);
		messageType = getIntent().getExtras().getString(ParseConstants.KEY_MESSAGE_TYPE);
		
		//show the first fragment
		if(savedInstanceState == null){
			getSupportFragmentManager().beginTransaction().add(fragmentContainer, 
					SolveImageFragment.newInstance(configOptions, mUriString, titleName, firstCaption)).commit();
		}
	}
	
	private Bundle getMergedOptions() {
		// Read activity metadata from AndroidManifest.xml
		ActivityInfo activityInfo = null;
		try {
			activityInfo = getPackageManager().getActivityInfo(
					this.getComponentName(), PackageManager.GET_META_DATA);
		} catch (NameNotFoundException e) {
//			if (Parse.getLogLevel() <= Parse.LOG_LEVEL_ERROR &&
//					Log.isLoggable(LOG_TAG, Log.WARN)) {
//				Log.w(LOG_TAG, e.getMessage());
//			}
		}

		// The options specified in the Intent (from ParseLoginBuilder) will
		// override any duplicate options specified in the activity metadata
		Bundle mergedOptions = new Bundle();
		if (activityInfo != null && activityInfo.metaData != null) {
			mergedOptions.putAll(activityInfo.metaData);
		}
		if (getIntent().getExtras() != null) {
			mergedOptions.putAll(getIntent().getExtras());
		}

		return mergedOptions;
	}

	@Override
	public void onImageSolved() {
		//called when an image is completed being solved, so start the viewing fragment.	
		
		//figure out what to do when an image is solved based on what type of puzzle it was
		if(messageType == ParseConstants.NEW_MESSAGE){
			//remove from the message recipients
			//save it to phone
		}else if(messageType == ParseConstants.FEATURED_MESSAGE){
			//add it to user's solved featured array
			//save it to phone
		}else if(messageType == ParseConstants.ADDITIONAL_MESSAGE){
			//add to user's solved string
		}
		
		FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
		transaction.replace(fragmentContainer,
				ViewImageFragment.newInstance(configOptions));
		transaction.addToBackStack(null);
		transaction.commit();
	}

	@Override
	public void onNextClicked() {
		//called when done viewing image, temporary
//		FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
//		transaction.replace(fragmentContainer,
//				SolveImageFragment.newInstance(configOptions));
//		transaction.addToBackStack(null);
//		transaction.commit();
		ImageActivity.this.finish();
	}

	@Override
	public void onSolveNextClicked() {
		// called when user skips a puzzle from the solve screen
	}

	@Override
	public void onViewFragmentBackClicked() {
		ImageActivity.this.finish();		
	}

	@Override
	public void onSolveBackClicked() {
		ImageActivity.this.finish();
	}
}
