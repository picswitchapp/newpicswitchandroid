package com.picswitch;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

public class ChangePasswordFragment extends Fragment {

	Context mContext;
	EditText et1;
	EditText et2;
	EditText et3;
	InputMethodManager imm;

	public interface ChangePasswordFragmentListener{
		public void onChangePasswordBackClicked();
		public void onChangePasswordConfirmClicked(String currentPass, String newPass1, String newPass2);
	}

	private ChangePasswordFragmentListener mChangePassListener;

	public ChangePasswordFragment() {
	}

	public static ChangePasswordFragment newInstance(Bundle configOptions){
		ChangePasswordFragment passFrag = new ChangePasswordFragment();
		Bundle args = new Bundle(configOptions);
		passFrag.setArguments(args);
		return passFrag;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View mView;
		mView = inflater.inflate(R.layout.change_password_fragment, container, false);
		mContext = container.getContext();
		imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);

		et1 = (EditText) mView.findViewById(R.id.old_password);
		et2 = (EditText) mView.findViewById(R.id.new_password);
		et3 = (EditText) mView.findViewById(R.id.confirm_new_password);

		ImageView leftArrow = (ImageView) mView.findViewById(R.id.left_arrow_change_pass);
		leftArrow.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				imm.hideSoftInputFromWindow(et1.getWindowToken(), 0);
				imm.hideSoftInputFromWindow(et2.getWindowToken(), 0);
				imm.hideSoftInputFromWindow(et3.getWindowToken(), 0);
				mChangePassListener.onChangePasswordBackClicked();
			}
		});

		//handle changing of passwords
		RelativeLayout changePassConfirmButton = (RelativeLayout) mView.findViewById(R.id.confirm_pass_button);
		changePassConfirmButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				String currentPass = et1.getText().toString().trim();
				String newPassword = et2.getText().toString().trim();
				String confirmNewPassword = et3.getText().toString().trim();
				mChangePassListener.onChangePasswordConfirmClicked(currentPass, newPassword, confirmNewPassword);
			}
		});

		return mView;
	}

	@Override
	public void onAttach(Activity activity){
		super.onAttach(activity);
		if(activity instanceof ChangePasswordFragmentListener){
			mChangePassListener = (ChangePasswordFragmentListener) activity;
		}else{
			throw new IllegalArgumentException(
					"Activity must implement ChangePasswordFragmentListener");
		}
	}

}
