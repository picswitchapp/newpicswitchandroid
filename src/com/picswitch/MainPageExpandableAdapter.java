package com.picswitch;

import java.util.List;
import java.util.Map;

import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.squareup.picasso.Picasso;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class MainPageExpandableAdapter extends BaseExpandableListAdapter {

	private Context mContext;
	private Map<String, List<ParseObject>> mainMap;
	private List<String> headerNames;
	private LayoutInflater mInflater;

	private Typeface montserrat;
	private Typeface varella;

	public MainPageExpandableAdapter(Context mContext, Map<String, List<ParseObject>> mainMap, List<String> headerNames){
		this.mContext = mContext;
		this.mainMap = mainMap;
		this.headerNames = headerNames;
		mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		montserrat = Typeface.createFromAsset(mContext.getAssets(), "Montserrat-Regular.ttf");
		varella = Typeface.createFromAsset(mContext.getAssets(), "VarelaRound.ttf");
	}

	@Override
	public int getGroupCount() {
		return headerNames.size();
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		return mainMap.get(headerNames.get(groupPosition)).size();
	}

	@Override
	public Object getGroup(int groupPosition) {
		return headerNames.get(groupPosition);
	}

	@Override
	public Object getChild(int groupPosition, int childPosition) {
		return mainMap.get(headerNames.get(groupPosition)).get(childPosition);
	}

	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}

	@Override
	public boolean hasStableIds() {
		return true;
	}

	public static class ChildViewHolder{
		public RelativeLayout mainLayout;
		public TextView leftBar;
		public TextView senderName;
		public TextView message;
		public ImageView messageIcon;
		public int position;
	}

	public static class GroupViewHolder{
		public TextView headerText;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
		String groupName = (String) getGroup(groupPosition);

		View mView = convertView;
		GroupViewHolder holder;

		if(mView == null){
			mView = mInflater.inflate(R.layout.main_list_view_title, null);

			TextView title = (TextView) mView.findViewById(R.id.main_list_view_title_text);
			title.setTypeface(montserrat);

			holder = new GroupViewHolder();
			holder.headerText = title;

			mView.setTag(holder);
		}else{
			holder = (GroupViewHolder) mView.getTag();
		}

		holder.headerText.setText(groupName);

		return mView;
	}

	@Override
	public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

		final ParseObject mObject = (ParseObject) getChild(groupPosition, childPosition);

		View mView = convertView;
		ChildViewHolder holder;

		if(mView == null){
			mView = mInflater.inflate(R.layout.message_item, null);

			RelativeLayout mainLayout = (RelativeLayout) mView.findViewById(R.id.message_main_layout);
			TextView dateLabel = (TextView) mView.findViewById(R.id.dateLabel);
			TextView senderLabel = (TextView) mView.findViewById(R.id.senderLabel);
			TextView messageLabel  = (TextView) mView.findViewById(R.id.messageLabel);
			ImageView imageView = (ImageView) mView.findViewById(R.id.messageIcon);

			senderLabel.setTypeface(varella);
			messageLabel.setTypeface(varella);

			if(childPosition % 2 == 1){
				mainLayout.setBackgroundColor(Color.rgb(51, 51, 51));
				dateLabel.setBackgroundColor(Color.rgb(50, 153, 153));
			}else{
				mainLayout.setBackgroundColor(Color.rgb(77, 78, 79));
				dateLabel.setBackgroundColor(Color.rgb(0, 103, 102));
			}

			holder = new ChildViewHolder();
			holder.mainLayout = mainLayout;
			holder.leftBar = dateLabel;
			holder.senderName = senderLabel;
			holder.message = messageLabel;
			holder.messageIcon = imageView;
			holder.position = childPosition;

			mView.setTag(holder);
		}else{
			holder = (ChildViewHolder) mView.getTag();
			if(childPosition % 2 == 1){
				holder.mainLayout.setBackgroundColor(Color.rgb(51, 51, 51));
				holder.leftBar.setBackgroundColor(Color.rgb(50, 153, 153));
			}else{
				holder.mainLayout.setBackgroundColor(Color.rgb(77, 78, 79));
				holder.leftBar.setBackgroundColor(Color.rgb(0, 103, 102));
			}
		}

		if(groupPosition == 0){

			if(mObject.getString(ParseConstants.CAPTION_FIRST).length()==0||mObject.getString(ParseConstants.CAPTION_FIRST)==null){
				try {
					holder.senderName.setText(mObject.getParseUser(ParseConstants.KEY_SENDER).fetchIfNeeded().getString(ParseConstants.KEY_FIRST_NAME) + " "
							+ mObject.getParseUser(ParseConstants.KEY_SENDER).fetchIfNeeded().getString(ParseConstants.KEY_LAST_NAME));
				} catch (ParseException e1) {
					System.out.println("e1 exception");
					e1.printStackTrace();
				}
			}
			else{
				try {
					holder.senderName.setText(mObject.getParseUser(ParseConstants.KEY_SENDER).fetchIfNeeded().getString(ParseConstants.KEY_FIRST_NAME) + " "
							+ mObject.getParseUser(ParseConstants.KEY_SENDER).fetchIfNeeded().getString(ParseConstants.KEY_LAST_NAME));
				} catch (ParseException e2) {
					System.out.println("e2 exception");
					e2.printStackTrace();
				}
				holder.message.setText(mObject.getString(ParseConstants.CAPTION_FIRST));
			}
		}else if(groupPosition == 1){

			holder.senderName.setText(mObject.getString(ParseConstants.KEY_COMPANY_NAME));
			holder.message.setText(mObject.getString(ParseConstants.CAPTION_FIRST));

		}

		ParseFile mFile = mObject.getParseFile(ParseConstants.KEY_PHOTO);
		Uri mUri = Uri.parse(mFile.getUrl());
		Picasso.with(mContext).load(mUri).placeholder(R.drawable.picasso_loading_animation)
		.transform(new BitmapTransform( (Integer)mObject.getNumber(ParseConstants.KEY_PREVIEW_INDEX) ))
		.into(holder.messageIcon);

		return mView;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return true;
	}

}














