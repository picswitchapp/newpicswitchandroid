package com.picswitch;

import com.flurry.android.FlurryAgent;
import com.parse.Parse;
import com.parse.ParseCrashReporting;
import com.parse.ParseInstallation;
import com.parse.PushService;

/**Application class that initializes app data like 
 * push
 * connection to parse
 * Executed first*/
public class Application extends android.app.Application {
	/**Default constructor necessary*/
	public Application() {
	}

	@Override
	public void onCreate() {
		super.onCreate();
		
		//Initialize the Parse SDK.
		//has the app ID and the client ID
		
		ParseCrashReporting.enable(this);
		
		Parse.initialize(this, "T0mfW1Uz1wCR2l4xZBipnt36S4svUDrMfunBHcDA", "zTz4VHrKcOAjtPbVhLCpy7bif6IZSF295FCe9UMu"); 
		ParseInstallation.getCurrentInstallation().saveInBackground();
		Parse.setLogLevel(Parse.LOG_LEVEL_DEBUG);
		FlurryAgent.init(this, "XZCRQ48ZVH6RWWHN447V");
	    //PushService.setDefaultPushCallback(this, TabActivity.class);
	}
	
}
