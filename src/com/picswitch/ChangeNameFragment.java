package com.picswitch;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.parse.ParseUser;

/**
 * A simple {@link Fragment} subclass.
 *
 */
public class ChangeNameFragment extends Fragment {
	
	Context mContext;
	EditText firstEditText;
	EditText secondEditText;
	InputMethodManager imm;
	
	public interface ChangeNameFragmentListener{
		public void onChangeNameFragmentBackClicked();
		public void onConfirmNameChangeClicked(String firstName, String lastName);
	}
	
	private ChangeNameFragmentListener mChangeNameListener;

	public ChangeNameFragment() {
	}
	
	public static ChangeNameFragment newInstance(Bundle configOptions){
		ChangeNameFragment nameFrag = new ChangeNameFragment();
		Bundle args = new Bundle(configOptions);
		nameFrag.setArguments(args);
		return nameFrag;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View mView;
		mView = inflater.inflate(R.layout.change_name_fragment, container, false);
		mContext = container.getContext();
		imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
		
		firstEditText = (EditText) mView.findViewById(R.id.first_name);
		secondEditText = (EditText) mView.findViewById(R.id.last_name);
		firstEditText.setHint(ParseUser.getCurrentUser().getString(ParseConstants.KEY_FIRST_NAME));
		secondEditText.setHint(ParseUser.getCurrentUser().getString(ParseConstants.KEY_LAST_NAME));
		
		ImageView leftArrow = (ImageView) mView.findViewById(R.id.left_arrow_edit_profile);
		leftArrow.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				imm.hideSoftInputFromWindow(firstEditText.getWindowToken(), 0);
				imm.hideSoftInputFromWindow(secondEditText.getWindowToken(), 0);
				mChangeNameListener.onChangeNameFragmentBackClicked();
			}
		});
		
		RelativeLayout confirmButton = (RelativeLayout) mView.findViewById(R.id.edit_profile_confirm_button);
		confirmButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				String newFirstName = firstEditText.getText().toString().trim();
				String newLastName = secondEditText.getText().toString().trim();
				mChangeNameListener.onConfirmNameChangeClicked(newFirstName, newLastName);
			}
		});
		
		return mView;
	}
	
	@Override
	public void onAttach(Activity activity){
		super.onAttach(activity);
		if(activity instanceof ChangeNameFragmentListener){
			mChangeNameListener = (ChangeNameFragmentListener) activity;
		}else{
			throw new IllegalArgumentException(
					"Activity must implement ChangeNameFragmentListener");
		}
	}

}
