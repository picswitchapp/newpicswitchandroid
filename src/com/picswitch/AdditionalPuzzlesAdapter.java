package com.picswitch;

import java.util.List;

import com.parse.ParseFile;
import com.parse.ParseObject;
import com.squareup.picasso.Picasso;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class AdditionalPuzzlesAdapter extends ArrayAdapter<ParseObject> {

	private Context mContext;
	private List<ParseObject> mCategories;
	private LayoutInflater mInflater;
	
	private Typeface montserrat;
	private Typeface varella;

	private boolean isQuerying = true;

	public AdditionalPuzzlesAdapter(Context mContext, List<ParseObject> list){
		super(mContext, R.layout.message_item);
		this.mContext = mContext;
		mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		mCategories = list;
		
		montserrat = Typeface.createFromAsset(mContext.getAssets(), "Montserrat-Regular.ttf");
		varella = Typeface.createFromAsset(mContext.getAssets(), "VarelaRound.ttf");
	}

	@Override
	public int getCount() {
		return mCategories.size();
	}

	@Override
	public ParseObject getItem(int position) {
		// TODO Auto-generated method stub
		return mCategories.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	public static class ViewHolder{
		public RelativeLayout mainLayout;
		public TextView leftBar;
		public TextView categoryTitle;
		public TextView categoryDescription;
		public ImageView categoryImage;
		public int position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		View mView = convertView;
		ViewHolder holder;

		if(mView == null){

			mView = mInflater.inflate(R.layout.message_item, null);
			
			RelativeLayout mainLayout = (RelativeLayout) mView.findViewById(R.id.message_main_layout);
			TextView dateLabel = (TextView) mView.findViewById(R.id.dateLabel);
			
			TextView title = (TextView) mView.findViewById(R.id.senderLabel);
			TextView desc  = (TextView) mView.findViewById(R.id.messageLabel);
			ImageView imageView = (ImageView) mView.findViewById(R.id.messageIcon);
			
			title.setTypeface(varella);
			desc.setTypeface(varella);
			
			if(position % 2 == 1){
				mainLayout.setBackgroundColor(Color.rgb(51, 51, 51));
				dateLabel.setBackgroundColor(Color.rgb(50, 153, 153));
			}else{
				mainLayout.setBackgroundColor(Color.rgb(77, 78, 79));
				dateLabel.setBackgroundColor(Color.rgb(0, 103, 102));
			}

			holder = new ViewHolder();
			holder.mainLayout = mainLayout;
			holder.leftBar = dateLabel;
			holder.categoryTitle = title;
			holder.categoryDescription = desc;
			holder.categoryImage = imageView;
			holder.position = position;

			mView.setTag(holder);
		}else{
			holder = (ViewHolder) mView.getTag();
			if(position % 2 == 1){
				holder.mainLayout.setBackgroundColor(Color.rgb(51, 51, 51));
				holder.leftBar.setBackgroundColor(Color.rgb(50, 153, 153));
			}else{
				holder.mainLayout.setBackgroundColor(Color.rgb(77, 78, 79));
				holder.leftBar.setBackgroundColor(Color.rgb(0, 103, 102));
			}
		}

		holder.categoryTitle.setText(mCategories.get(position).getString(ParseConstants.KEY_CATEGORY_NAME));
		holder.categoryDescription.setText(mCategories.get(position).getString(ParseConstants.KEY_CATEGORY_DESCRIPTION));

		ParseFile mFile = mCategories.get(position).getParseFile(ParseConstants.KEY_PHOTO);
		Uri mUri = Uri.parse(mFile.getUrl());
		Picasso.with(mContext).load(mUri).placeholder(R.drawable.picasso_loading_animation)
		.into(holder.categoryImage);


		return mView;
	}

}
